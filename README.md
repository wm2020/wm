# README #

### Working with non-master branches on staging

We need to push to the master branch to run code on the staging site. To push a non-master branch to staging, do the following:

    $ git push staging my-branch-name:master
	
To pull staging to a non-master branch, do the following:

    $ git pull staging master:my-branch-name

### Pushing live

We should only ever be pushing the master branch to the production site. Master should always be production-ready.

    $ git checkout master
    $ git merge branch-name-here
    $ git push production master