<?php

class Cache {
    function __construct($key, $seconds) {
      $this->key = $key;
      $this->seconds = $seconds;
    }

    function cacheFilePath() {
      return dirname(__FILE__) . $this->key . ".txt";
    }

    function hasContent() {
        return $this->cacheFileExists();
    }

    function cacheFileExists() {
        return file_exists($this->cacheFilePath());
    }

    function loadContent() {
        return file_get_contents($this->cacheFilePath());
    }

    function cacheContent($data) {
        $this->clear();
        file_put_contents($this->cacheFilePath(), $data);
    }

    function clearExpired() {
        $cacheFilePath = $this->cacheFilePath();

        if($this->cacheFileExists()) {
            $timestamp = filemtime($cacheFilePath);
            $age = time() - $timestamp;
            if($age > $this->seconds) {
                unlink($cacheFilePath);
            }
        }

        return true;
    }

    function clear() {
        if($this->cacheFileExists()) {
            unlink($this->cacheFilePath());
        }

        return true;
    }


    function expired() {
        $cacheFilePath = $this->cacheFilePath();

        if($this->cacheFileExists()) {
            $timestamp = filemtime($cacheFilePath);
            $age = time() - $timestamp;
            return ($age > $this->seconds);
        }

        return true;
    }
}