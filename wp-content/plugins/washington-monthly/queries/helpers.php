<?php

/** PARTIAL QUERIES **/
function _query_with_term_id($term_id, $taxonomy = 'category') {
  return array(
    'taxonomy' => $taxonomy,
    'field' => 'id',
    'terms' => $term_id
  );
}

function _query_without_term_name($name, $taxonomy = 'category') {
  return array(
    'taxonomy' => $taxonomy,
    'field' => 'name',
    'terms' => array($name),
    'operator' => 'NOT IN'
  );
}

function _query_with_term_name($name, $taxonomy = 'category') {
  return array(
    'taxonomy' => $taxonomy,
    'field' => 'name',
    'terms' => array($name),
    'operator' => 'IN',
    'include_children' => false

  );
}

function _query_with_featured_image() {
  return array(
    'key' => '_thumbnail_id',
    'compare' => 'EXISTS'
  );
}
/** END PARTIAL QUERIES **/

function _post_ids_with_same_tags($post, $limit = 3, $exclude_posts = array()) {
  $tag_ids = array_map(
    function($tag) { return $tag->term_id; }, 
    wp_get_post_tags($post->ID)
  );

  $exclude_posts[] = $post->ID;

  $q = new WP_Query(array(
      'post__not_in' => $exclude_posts,
      'tag__in' => $tag_ids,
      'posts_per_page' => $limit,
      'fields' => 'ids'
    ));

  return $q->get_posts();
}

function _post_ids_in_same_categories($post, $limit = 3, $exclude_posts = array()) {
  $exclude_posts[] = $post->ID;

  $category_ids = array_map(
    function($category) { return $category->term_id; },
    get_the_category($post->ID)
  );

  $q = new WP_Query(array(
      'post__not_in' => $exclude_posts,
      'category__in' => $category_ids,
      'posts_per_page' => $limit,
      'fields' => 'ids'
    ));

  return $q->get_posts();
}

function _post_ids_by_same_authors($post, $limit = 3, $exclude_posts = array()) {
  $exclude_posts[] = $post->ID;

  $author_ids = array_map(
    function($author) { return $author->ID; }, 
    get_field('author')
  );

  $author_meta = array('relation' => 'OR');

  foreach($author_ids as $author_id) {
    $author_meta[] = array(
      'key' => 'author',
      'value' => "i:{$author_id};",
      'compare' => 'LIKE'
    );

    $author_meta[] = array(
      'key' => 'author',
      'value' => "\"{$author_id}\";",
      'compare' => 'LIKE'
    );
  }

  $q = new WP_Query(array(
      'posts_per_page' => $limit,
      'post__not_in' => $exclude_posts,
      'post_type' => 'post',
      'fields' => 'ids',
      'meta_query' => $author_meta
    ));

  return $q->get_posts();
}

