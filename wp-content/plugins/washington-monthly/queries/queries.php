<?php

require_once('helpers.php');

function query_sticky_category_post($term_id, $offset = 0)
{
    return new WP_Query(
        array(
      'posts_per_page' => 1,
      'paged' => 0,
      'offset' => $offset,
      'meta_query' => array(
        _query_with_featured_image()
        ),
      'tax_query' => array(
        'relation' => 'AND',
        _query_with_term_id($term_id),
        _query_without_term_name('Hide Category Highlight Sticky'),
        )
      )
    );
}

function get_pa_offset($offset = 3)
{
    $featured = query_main_featured_post();
    $featured_posts = $featured->posts;
    foreach ($featured_posts as $post) {
        if (in_category(4, $post)) {
            $offset++;
        }
    }
    return $offset;
}

function query_highlighted_category_posts($term_id, $skip_sticky = true, $offset = 0)
{
    $args = array(
    'posts_per_page' => 5,
    'paged' => 0,
    'offset' => $offset,
    'tax_query' => array(
      _query_with_term_id($term_id),
      _query_without_term_name('Hide Category Highlight'),
      )
    );

    if ($skip_sticky) {
        $sticky = query_sticky_category_post($term_id, $offset);
        if ($sticky->have_posts()) {
            $sticky->the_post();
            $args['post__not_in'] = [get_the_id()];
        }
    }

    return new WP_Query($args);
}

/**
 * Custom query for posts in the Cover category for the given issue
 */
function query_issue_cover($term_id)
{
    return new WP_Query(
        array(
      'posts_per_page' => -1,
      'tax_query' => array(
        array(
          'taxonomy' => 'category',
          'field' => 'name',
          'terms' => array('Cover'),
          'operator' => 'IN',
          ),
        array(
          'taxonomy' => 'issues',
          'field' => 'id',
          'terms' => $term_id,
          )
        )
      )
    );
}

/**
 * Custom query for non-Cover posts in a given issue
 */
function query_issue_archive($term_id)
{
    return new WP_Query(
        array(
      'posts_per_page' => -1,
      'tax_query' => array(
        'relation' => 'AND',
        _query_without_term_name('Cover'),
        _query_with_term_id($term_id, 'issues')
        )
      )
    );
}

/**
 * Returns a custom query that finds and paginates
 * posts by a given author.
 */
function query_posts_by_author($author_id, $page)
{
    return new WP_Query(
        array(
      'paged' => $page,
      'posts_per_page' => 5,
      'post_type' => 'post',
      'meta_query' => array(
        'relation' => 'OR',
        array(
          'key' => 'author',
          'value' => "i:{$author_id};",
          'compare' => 'LIKE'
          ),
        array(
          'key' => 'author',
          'value' => "\"{$author_id}\";",
          'compare' => 'LIKE'
          )
        )
      )
    );
}

/**
 * Returns a custom query that finds the main featured post.
 */
function query_main_featured_post()
{
    return new WP_Query(
        array(
      'posts_per_page' => 1,
      'cat' => 78,
      'orderby' => 'post_date',
      'order' => 'DESC',
      'post_type' => array('post', 'page'),
      'meta_query' => array(
            array(
            'key' => '_thumbnail_id',
            'compare' => 'EXISTS'
            ),
        )
      )
    );
}

/**
 * Returns a custom query that finds some recent posts.
 */
function query_recent_posts()
{
    $args = array(
      'posts_per_page' => 3,
      'orderby' => 'post_date',
      'order' => 'DESC',
      'category__in' => [4],
      'tax_query' => array(
        'relation' => 'AND',
        _query_without_term_name('Main'),
        ),
   );

    return new WP_Query($args);
}

/**
 * Custom query that finds featured posts other than the main featured post
 */
function query_more_featured_posts()
{
    return new WP_Query(
        array(
      'posts_per_page' => 4,
      'tax_query' => array(
        'relation' => 'AND',
        _query_with_term_name('Featured'),
        _query_without_term_name('Main'),
        ),
      'orderby' => 'post_date',
      'order' => 'DESC',
      )
    );
}

/**
 * Custom query to find most-viewed recent posts
 */
function query_most_popular_posts($count = 5, $days_ago = 7)
{
    $query = new WP_Query(
        array(
      'posts_per_page' => $count,
      'meta_key' => '_gapp_post_views',
      'orderby' => 'meta_value_num DESC, post_date DESC',
      'date_query' => array(array('after' => "{$days_ago} days ago"))
      )
  );

    if (!$query->have_posts()) {
        $query = new WP_Query(
            array('posts_per_page' => $count, 'orderby' => 'post_date DESC')
    );
    }

    return $query;
}


/**
 * Custom query to find posts in the From The Archives category
 */
function query_from_the_archives()
{
    return new WP_Query(
        array(
      'posts_per_page' => 5,
      'tax_query' => array(
        'relation' => 'AND',
        _query_with_term_name('From The Archives')
      )
    )
  );
}


function query_related_posts()
{
    global $post;
    $limit = 3;

    $post_ids = _post_ids_with_same_tags($post, $limit);
    $remaining = $limit - count($post_ids);

    if ($remaining > 0) {
        $post_ids = array_merge(
            $post_ids,
            _post_ids_by_same_authors($post, $remaining, $post_ids)
      );
        $remaining = $limit - count($post_ids);
    }

    if ($remaining > 0) {
        $post_ids = array_merge(
            $post_ids,
            _post_ids_in_same_categories($post, $remaining, $post_ids)
      );
    }

    return new WP_Query(array('post__in' => $post_ids, 'posts_per_page' => $limit));
}
