<?php

/**
 * Add Custom Options link to the wp-admin menu
 */
function wm_admin_options_page() {
    add_options_page('WM Options', 'WM Options', 'manage_options',
                     'functions','wm_admin_options_form');

    add_action("admin_init", "wm_register_options");
}
add_action('admin_menu', 'wm_admin_options_page');


/**
 * Display the custom options form
 */
function wm_admin_options_form() {
  include_once('options_form.html.php');
}

function wm_register_options() {
    register_setting("wm-options-group", "wm_current_issue");
    register_setting("wm-options-group", "wm_homepage_categories");
    register_setting("wm-options-group", "wm_ad_display");
}

/**
 * Add styles to the WYSIWYG
 */
function wm_modify_wysiwyg($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'wm_modify_wysiwyg');

function wm_add_styles_to_wysiwyg($init_array) {
    $style_formats = array(
        array(
            'title' => 'Blue Button',
            'block' => 'span',
            'classes' => 'btn btn--blue',
            'wrapper' => true,
        ),
        array(
            'title' => 'Red Button',
            'block' => 'span',
            'classes' => 'btn btn--red',
            'wrapper' => true,
        ),
        array(
            'title' => 'White Button',
            'block' => 'span',
            'classes' => 'btn btn--white',
            'wrapper' => true,
        )
    );

    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;
}
// Attach callback to 'tiny_mce_before_init'
add_filter('tiny_mce_before_init', 'wm_add_styles_to_wysiwyg');

function wm_add_editor_styles() {
    add_editor_style('css/custom-editor-style.css');
}
add_action('init', 'wm_add_editor_styles');