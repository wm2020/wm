<div class="wrap">
  <h2>WM Options</h2>

  <form method="post" action="options.php">
    <?php settings_fields('wm-options-group'); ?>
    <?php do_settings_sections('wm-options-group'); ?>

    <h3>Current Issue</h3>
    <p>
      <?php wp_dropdown_categories(array(
            'hide_empty' => false,
      'taxonomy' => 'issues',
      'name' => 'wm_current_issue',
      'selected' => get_option('wm_current_issue'),
      )); ?>
    </p>

    <h3>Homepage Categories</h3>
    <p>Select the categories for which homepage blocks should display.</p>
    <p>
      <?php $cats = get_categories(); ?>
      <?php $selected = get_option('wm_homepage_categories'); ?>
      <?php if(!is_array($selected)) $selected = array(); ?>

      <?php foreach($cats as $cat) : ?>
      <input type="checkbox"
             name="wm_homepage_categories[]"
             value="<?php echo $cat->term_id; ?>"
             <?php if(in_array($cat->term_id, $selected)) { echo "checked"; } ?>
             > <?php echo $cat->name; ?><br>
	     <?php endforeach; ?>
    </p>

    <h3>Ad Display</h3>
    <?php $ad_display = get_option('wm_ad_display'); ?>
    <p>
      <select name="wm_ad_display" id="wm_ad_display" class="postform">
        <option value="hide" <?php if($ad_display == 'hide') echo 'selected'; ?>>Hide Ads</option>
        <option value="placeholder" <?php if($ad_display == 'placeholder') echo 'selected'; ?>>Placeholder Images</option>
        <option value="google" <?php if($ad_display == 'google') echo 'selected'; ?>>Google Ads</option>
	<option value="adkarma" <?php if($ad_display == 'adkarma') echo 'selected'; ?>>AdKarma Ads</option>
        <option value="revive" <?php if($ad_display == 'revive') echo 'selected'; ?>>Revive Ads</option>
      </select>
    </p>

    <?php submit_button(); ?>
  </form>
</div>
