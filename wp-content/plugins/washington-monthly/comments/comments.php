<?php

/**
 * Legacy code library used to convert a string into a valid
 * directory name. This is required to generate the correct id
 * for Disqus comments.
 */
require_once('dirify.php');


/**
 * Generate disqus id from the post title and date.
 */
function get_the_disqus_id() {
    $disqus_id = dirify(get_the_title());

    $start_date = strtotime('2012-01-25'); // political animal 2012 start date
    $post_date  = strtotime(get_the_date('Y-m-d')); // publication date

    if($post_date >= $start_date && in_category('Political Animal Blog')) {
        $disqus_id .= '_' . get_the_date('m_d_Y');
    }

    return $disqus_id;
}


/**
 * Output the disqus id for the current post.
 */
function the_disqus_id() {
  echo get_the_disqus_id();
}