<?php

class ReviveAds extends AdProvider {
  const REVIVE_BASE_URL = '52.44.78.233';
  const REVIVE_ID = '99d2c205139bd499348c2b1f14fc8e47';
  const REVIVE_JS_URL = '//52.44.78.233/revive/www/delivery/asyncjs.php';

  var $codes = [
    'square-1' => 1,
    'square-2' => 2,
    'square-3' => 3,
    'square-4' => 4,
    'square-5' => 5,
    'square-6' => 6,
    'square-7' => 7,
    'top-banner-large' => 8,
    'content-banner' => 10
  ];

  // Old. Used for asynch js
  function xgetAdCode($key) {
    return sprintf(
      $this->template(),
      $this->codes[$key],
      self::REVIVE_ID,
      self::REVIVE_JS_URL
    );
  }

  function getAdCode($key) {
    return sprintf(
      $this->template(),
      $this->codes[$key],
      $this->codes[$key]
    );
  }

  private function template() {
    $url = self::REVIVE_BASE_URL;
    ob_start(); ?>
    <script type='text/javascript'><!--//<![CDATA[
				   var m3_u = (location.protocol=='https:'?'https://<?php echo $url; ?>/revive/www/delivery/ajs.php':'http://<?php echo $url; ?>/revive/www/delivery/ajs.php');
     var m3_r = Math.floor(Math.random()*99999999999);
     if (!document.MAX_used) document.MAX_used = ',';
     document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
     document.write ("?zoneid=%d");
     document.write ('&amp;cb=' + m3_r);
     if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
     document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
     document.write ("&amp;loc=" + escape(window.location));
     if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
     if (document.context) document.write ("&context=" + escape(document.context));
     if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
     document.write ("'><\/scr"+"ipt>");
     //]]>--></script><noscript><a href='http://<?php echo $url; ?>/revive/www/delivery/ck.php?n=a12aa275&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://<?php echo $url; ?>/revive/www/delivery/avw.php?zoneid=%d&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a12aa275' border='0' alt='' /></a></noscript>
  <?php
  $code = ob_get_contents();
  ob_end_clean();
  return $code;
  }

  // Old method for asynchronous ads. Doesn't work with JCM.
  private function xtemplate() {
    return "<span class='ad'><ins data-revive-zoneid=\"%d\" data-revive-id=\"%s\"></ins></span>
    <script async src=\"%s\"></script>";
  }
  }
