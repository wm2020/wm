<?php

/**
* The minimum length a post must be for an ad to display
*/
define("MIN_WORD_COUNT_FOR_ADS", 800);
define("SHOW_AD_AFTER_WORDS", 350);

require_once('ad_provider.php');
require_once('revive_ads.php');
require_once('ad_karma_ads.php');
require_once('google_ads.php');
require_once('placeholder_ads.php');
require_once('hidden_ads.php');

/**
* Determine if a post is long enough for an ad to be embedded
*/
function wm_ads_should_ad_be_embedded() {
  return false;

  $content = get_the_content();
  $word_count = str_word_count(strip_tags($content));

  return $word_count >= MIN_WORD_COUNT_FOR_ADS;
}

function wm_content_length() {
  $content = get_the_content();
  return str_word_count(strip_tags($content));
}

/**
* Output the post content with an ad embedded in it.
* If the post isn't long enough, just output the content.
*/
function the_content_with_ads($length = null, $after = null) {
  ob_start();
  the_content();
  $content = ob_get_clean();

  if($length == null) $length = MIN_WORD_COUNT_FOR_ADS;
  if($after == null) $after = SHOW_AD_AFTER_WORDS;

  if(wm_content_length() < $length) {
    echo $content;
    return;
  }

  $doc = new DOMDocument();
  libxml_use_internal_errors(true);
  $doc->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
  $xpath = new DOMXPath($doc);

  // break content on block quotes and paragraphs
  $nodes = $xpath->query('//*[self::blockquote or self::p]');
  $word_count = 0;

  foreach($nodes as $node) {
    $word_count += str_word_count($node->textContent);

    if($word_count >= $after) {
      $placeholder = $doc->createDocumentFragment();
      $placeholder->appendXml('WM_AD_PLACEHOLDER');
      $node->parentNode->insertBefore($placeholder, $node->nextSibling);
      break;
    }
  }

  $content = $doc->saveHTML();

  $ad = wm_get_ad_code('content-banner');

  $content = preg_replace('/WM_AD_PLACEHOLDER/', $ad, $content);

  echo $content;
}

function wm_ad_code($key) {
  echo wm_get_ad_code($key);
}

function wm_get_ad_provider() {
  $provider = get_option('wm_ad_display');

  if(isset($_GET['ad-provider'])) {
    $provider = $_GET['ad-provider'];
  }

  switch($provider) {
    case 'revive':
    return new ReviveAds();
    case 'adkarma':
    return new AdKarmaAds();
    case 'google':
    return new GoogleAds();
    case 'placeholder':
    return new PlaceholderAds();
    default:
    return new HiddenAds();
  }
}

function wm_get_ad_code($size) {
  $provider = wm_get_ad_provider();
  return $provider->getAdMarkup($size);
}

function wm_get_ad_footer_markup() {
  $provider = wm_get_ad_provider();
  return $provider->getFooterMarkup();
}
