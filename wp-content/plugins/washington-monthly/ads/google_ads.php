<?php

class GoogleAds extends AdProvider {

  var $codes = [
    'square-2' => ['width' => '300', 'height' => '250', 'slot' => '9273196063'],
    'square-3' => ['width' => '300', 'height' => '250', 'slot' => '9273196063'],
    'square-4' => ['width' => '300', 'height' => '250', 'slot' => '9273196063'],
    'square-5' => ['width' => '300', 'height' => '250', 'slot' => '9273196063'],
    'top-banner-large' => ['width' => '728', 'height' => '90',  'slot' => '8678428688'],
    'content-banner' => ['width' => '728', 'height' => '90',  'slot' => '8678428688'],
    '160x600' => ['width' => '160', 'height' => '600', 'slot' => '9542696179'],
    '300x250' => ['width' => '300', 'height' => '250', 'slot' => '9273196063'],
    '728x90'  => ['width' => '728', 'height' => '90',  'slot' => '8678428688'],
    '300x600' => ['width' => '300', 'height' => '600', 'slot' => '6253461048']
  ];

  function getAdCode($size) {
    return sprintf(
      $this->template(),
      $this->codes[$size]['width'],
      $this->codes[$size]['height'],
      $this->codes[$size]['slot']
    );
  }

  private function template() {
    return '<ins class="adsbygoogle"
    style="display:block;width:%spx;height:%spx"
    data-ad-client="ca-pub-9037807250275212"
    data-ad-slot="%s"></ins>';
  }

  function getFooterMarkup() {
    ob_start(); ?>
    <!-- GoogleAds Footer Markup -->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script type="text/javascript">
      (function($) {
        $(document).ready(function() {
        	$(".adsbygoogle").each(function() {
        		(adsbygoogle = window.adsbygoogle || []).push({});
        	});
        });
      })(jQuery);
	  </script>
    
  <?php
    $markup = ob_get_contents();
    ob_end_clean();

    return $markup;
  }
}
