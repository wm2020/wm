<?php

class AdKarmaAds {
  var $codes = [
    '300x250' => ['aje_13452', 'aje_55270'],
    '728x90'  => ['aje_41982', 'aje_55268'],
    '160x600' => ['aje_41981', 'aje_55269'],
    '300x600' => ['aje_41983']
  ];
  
  function __construct() {
    if(!isset($_SESSION['adkarma'])) {
      $_SESSION['adkarma'] = [];
    }
  }

  function getAdMarkup($size) {
    if(!isset($this->codes[$size])) return '';
    
    foreach($this->codes[$size] as $code) {
      if(!in_array($code, $_SESSION['adkarma'])) {
        $_SESSION['adkarma'][] = $code;
	return "<div id='{$code}'></div>";
      }
    }

    return ''; // all ads at $size have been used
  }

  function getFooterMarkup() {
    ob_start(); ?>
    
    <?php if(in_array('aje_13452', $_SESSION['adkarma'])) : ?>
	<script type="text/javascript" language="JavaScript">
		aj_server = 'http://tag.1rx.io/rmp/'; aj_tagver = '1.0';
		aj_scripts = 'http://img.1rx.io/banners/';
		aj_zone = 'adkarma'; aj_adspot = '13452'; aj_page = '0'; aj_dim ='562'; aj_ch = ''; aj_ct = ''; aj_kw = '';
		aj_pv = true; aj_click = '';
	</script>
	<div id="aje_tmp_13452" style="display: none;"><script id="aje_scr_13452" type="text/javascript" language="JavaScript" src="http://img.1rx.io/banners/async-ajtg.js"></script></div>
	<?php endif; ?>

    <?php if(in_array('aje_41982', $_SESSION['adkarma'])) : ?>
	<script type="text/javascript" language="JavaScript">
		aj_server = 'http://tag.1rx.io/rmp/'; aj_tagver = '1.0';
		aj_scripts = 'http://img.1rx.io/banners/';
		aj_zone = 'adkarma'; aj_adspot = '41982'; aj_page = '0'; aj_dim ='728x90xD'; aj_ch = ''; aj_ct = ''; aj_kw = '';
		aj_pv = true; aj_click = '';
	</script>
	<div id="aje_tmp_41982" style="display: none;"><script id="aje_scr_41982" type="text/javascript" language="JavaScript" src="http://img.1rx.io/banners/async-ajtg.js"></script></div>
	<?php endif; ?>

    <?php if(in_array('aje_41981', $_SESSION['adkarma'])) : ?>
	<script type="text/javascript" language="JavaScript">
		aj_server = 'http://tag.1rx.io/rmp/'; aj_tagver = '1.0';
		aj_scripts = 'http://img.1rx.io/banners/';
		aj_zone = 'adkarma'; aj_adspot = '41981'; aj_page = '0'; aj_dim ='160x600xD'; aj_ch = ''; aj_ct = ''; aj_kw = '';
		aj_pv = true; aj_click = '';
	</script>
	<div id="aje_tmp_41981" style="display: none;"><script id="aje_scr_41981" type="text/javascript" language="JavaScript" src="http://img.1rx.io/banners/async-ajtg.js"></script></div>
	<?php endif; ?>

    <?php if(in_array('aje_41983', $_SESSION['adkarma'])) : ?>
	<script type="text/javascript" language="JavaScript">
		aj_server = 'http://tag.1rx.io/rmp/'; aj_tagver = '1.0';
		aj_scripts = 'http://img.1rx.io/banners/';
		aj_zone = 'adkarma'; aj_adspot = '41983'; aj_page = '0'; aj_dim ='300x600xD'; aj_ch = ''; aj_ct = ''; aj_kw = '';
		aj_pv = true; aj_click = '';
	</script>
	<div id="aje_tmp_41983" style="display: none;"><script id="aje_scr_41983" type="text/javascript" language="JavaScript" src="http://img.1rx.io/banners/async-ajtg.js"></script></div>
	<?php endif; ?>

    <?php if(in_array('aje_55269', $_SESSION['adkarma'])) : ?>
<script type="text/javascript" language="JavaScript"> 
  aj_server = 'http://tag.1rx.io/rmp/'; aj_tagver = '1.0'; 
  aj_scripts = 'http://img.1rx.io/banners/';
  aj_zone = 'adkarma'; aj_adspot = '55269'; aj_page = '0'; aj_dim ='160x600xD'; aj_ch = ''; aj_ct = ''; aj_kw = ''; 
  aj_pv = true; aj_click = ''; 
</script> 
<div id="aje_tmp_55269" style="display: none;"><script id="aje_scr_55269" type="text/javascript" language="JavaScript" src="http://img.1rx.io/banners/async-ajtg.js"></script></div>
<?php endif; ?>

    <?php if(in_array('aje_55270', $_SESSION['adkarma'])) : ?>
<script type="text/javascript" language="JavaScript"> 
  aj_server = 'http://tag.1rx.io/rmp/'; aj_tagver = '1.0'; 
  aj_scripts = 'http://img.1rx.io/banners/';
  aj_zone = 'adkarma'; aj_adspot = '55270'; aj_page = '0'; aj_dim ='562'; aj_ch = ''; aj_ct = ''; aj_kw = ''; 
  aj_pv = true; aj_click = ''; 
</script> 
<div id="aje_tmp_55270" style="display: none;"><script id="aje_scr_55270" type="text/javascript" language="JavaScript" src="http://img.1rx.io/banners/async-ajtg.js"></script></div>
<?php endif; ?>

    <?php if(in_array('aje_55268', $_SESSION['adkarma'])) : ?>
<script type="text/javascript" language="JavaScript"> 
  aj_server = 'http://tag.1rx.io/rmp/'; aj_tagver = '1.0'; 
  aj_scripts = 'http://img.1rx.io/banners/';
  aj_zone = 'adkarma'; aj_adspot = '55268'; aj_page = '0'; aj_dim ='728x90xD'; aj_ch = ''; aj_ct = ''; aj_kw = ''; 
  aj_pv = true; aj_click = ''; 
</script> 
<div id="aje_tmp_55268" style="display: none;"><script id="aje_scr_55268" type="text/javascript" language="JavaScript" src="http://img.1rx.io/banners/async-ajtg.js"></script></div>
<?php endif; ?>

      <?php
    $markup = ob_get_contents();
    ob_end_clean();

    $_SESSION['adkarma'] = [];

    return $markup;
  }
}