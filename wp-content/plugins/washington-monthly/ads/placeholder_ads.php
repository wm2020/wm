<?php

class PlaceholderAds extends AdProvider {
  var $codes = [
    'square-1' => [
      'size' => '300x250',
      'text' => 'Available_300x250'
    ],
    'square-2' => [
      'size' => '300x250',
      'text' => 'Google_300x250'
    ],
    'square-3' => [
      'size' => '300x250',
      'text' => 'Google_300x250'
    ],
    'square-4' => [
      'size' => '300x250',
      'text' => 'Available_300x250'
    ],
    'square-5' => [
      'size' => '300x250',
      'text' => 'Available_300x250'
    ],
    'square-6' => [
      'size' => '300x250',
      'text' => 'Available_300x250'
    ],
    'square-7' => [
      'size' => '300x250',
      'text' => 'Available_300x250'
    ],
    'top-banner-large' => [
      'size' => '728x90',
      'text' => 'Google_728x90'
    ],
    'content-banner' => [
      'size' => '728x90',
      'text' => 'Google_728x90'
    ],
    'mini-banner-1' => '160x75',
    'mini-banner-2' => '160x75',
    'mini-banner-3' => '160x75',
    'mini-banner-4' => '160x75',
    'mini-banner-5' => '160x75',
    'mini-banner-6' => '160x75',
    'mini-banner-7' => '160x75',
    'mini-banner-8' => '160x75',
    'mini-banner-9' => '160x75',
    'mini-banner-10' => '160x75',
    'mini-banner-11' => '160x75',
    'mini-banner-12' => '160x75',
    'mini-banner-13' => '160x75',
    'mini-banner-14' => '160x75',
    'mini-banner-15' => '160x75'
  ];

  function getAdMarkup($size) {
    $code = $this->codes[$size];
    if(!is_array($code)) {
      $text = $size;
      $dimensions = $size;
    } else {
      $text = $code['text'];
      $dimensions = $code['size'];
    }

    return "<img src='http://placehold.it/{$dimensions}?text={$text}' alt='ad' class='ad'>";
  }
}
