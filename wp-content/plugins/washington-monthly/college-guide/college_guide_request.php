<?php

class CollegeGuideRequest {
    const API_URL = "https://college-guide.herokuapp.com/api/";

    function __construct($endpoint, $attrs) {
        $this->attrs = $attrs;
        $this->endpoint = $endpoint;
        $this->path = null;
        $this->parseAttrs();
    }

    function request() {
        $cache = new CollegeGuideCache($this);
        $content = '';

        if($cache->expired()) {
            $content = $this->loadContent();
            if(!empty($content)) $cache->cacheContent($content);
        }

        // If the cache has content, use that
        if($cache->hasContent()) $content = $cache->loadContent();

        return $content;
    }

    function parseAttrs() {
        if(!array_key_exists("year", $this->attrs)) return false;
        if(!array_key_exists("type", $this->attrs)) return false;

        $this->year = $this->attrs["year"];
        $this->type = $this->attrs["type"];
        $this->path = "{$this->endpoint}/{$this->year}/{$this->type}";
    }

    function loadContent() {
        if ($this->path == null) return "";

        $content = @file_get_contents(self::API_URL . $this->path);

        if($content === false) return "";

        return $content;
    }
}