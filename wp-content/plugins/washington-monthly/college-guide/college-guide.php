<?php

require_once('college_guide_cache.php');
require_once('college_guide_request.php');

/**
 * Wrap content in a college guide div.
 */
function wm_college_guide_display($content) {
    $output = '<div class="wrapper"><div class="college-guide-embed">';
    $output.= $content;
    $output .= '</div></div>';
    return $output;
}


/**
 * Define custom short code for embedding college guide rankings
 */
function wm_college_guide_rankings_shortcode($attrs) {
    $api = new CollegeGuideRequest("rankings", $attrs);
    return wm_college_guide_display($api->request());
}
add_shortcode('college-guide-rankings', 'wm_college_guide_rankings_shortcode');


/**
 * Define custom short code for embedding best bang for buck rankings
 */
function wm_college_guide_best_bang_shortcode($attrs) {
    $api = new CollegeGuideRequest("best_bang_for_buck", $attrs);
    return wm_college_guide_display($api->request());
}
add_shortcode('college-guide-best-bang', 'wm_college_guide_best_bang_shortcode');
