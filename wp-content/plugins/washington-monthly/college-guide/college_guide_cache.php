<?php

class CollegeGuideCache {
    const CACHE_FOR_SECONDS = 86400;

    function __construct($request) {
        $this->request = $request;
    }

    function cacheFilePath() {
        $fileName = preg_replace("/\/|\_/", "-", $this->request->path) . ".html";
        return dirname(__FILE__) . "/cache/" . $fileName;
    }

    function hasContent() {
        return $this->cacheFileExists();
    }

    function cacheFileExists() {
        return file_exists($this->cacheFilePath());
    }

    function loadContent() {
        return file_get_contents($this->cacheFilePath());
    }

    function cacheContent($data) {
        $this->clear();
        file_put_contents($this->cacheFilePath(), $data);
    }

    function clearExpired() {
        $cacheFilePath = $this->cacheFilePath();

        if($this->cacheFileExists()) {
            $timestamp = filemtime($cacheFilePath);
            $age = time() - $timestamp;
            if($age > self::CACHE_FOR_SECONDS) {
                unlink($cacheFilePath);
            }
        }

        return true;
    }

    function clear() {
        if($this->cacheFileExists()) {
            unlink($this->cacheFilePath());
        }

        return true;
    }


    function expired() {
        $cacheFilePath = $this->cacheFilePath();

        if($this->cacheFileExists()) {
            $timestamp = filemtime($cacheFilePath);
            $age = time() - $timestamp;
            return ($age > self::CACHE_FOR_SECONDS);
        }

        return true;
    }
}