<?php
/*
Plugin Name: Washington Monthly
Description: Customizations for washingtonmonthly.com including custom post types and college guide integration.
*/
defined('ABSPATH') or die('Hello world!');

/**
 * Components are stored in subdirectories with the main include script named
 * after the directory (e.g. people/people.php).
 *
 * To display a component, remove it from the array below.
 */
$components = ['people', 'issues', 'college-guide', 'social', 'comments',
               'migrate', 'ads', 'contact', 'queries', 'admin', 'images',
	       'philanthropy', 'cache'];


/**
 * Require each component from the array above.
 */
foreach($components as $component) {
  require_once($component . '/' . $component . '.php');
}


/**
 * Override the Excerpt label in the post form so users
 * know that it's used to hold the subtitle.
 */
function wm_excerpt_label($translation, $original) {
  if ($original == 'Excerpt') {
    return 'Subtitle';
  }
  return $translation;
}
add_filter('gettext', 'wm_excerpt_label', 10, 2);


/**
 * Remove HTML tags, slashes, and short codes from a string.
 */
function wm_trim($value) {
    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_shortcodes($value);
    $value = strip_tags($value, '<a>');

    return $value;
}

function wm_pipe($value) {
  wm_trim($value);
  $value = " |" . $value;

  return $value;
}

add_filter('get_the_excerpt', 'wm_trim');
add_filter('wp_title', 'wm_pipe');


/**
 * Output a meta tag with a give name and content
 */
function meta_tag($name, $content, $name_attr = 'name') {
  echo "<meta {$name_attr}=\"$name\" content=\"{$content}\" />\n";
}


/**
 * Load a custom field value from the current post.
 * Return a default value if the field is not set.
 */
function wm_get_field($name, $default = '') {
  $result = get_field($name, get_the_id());

  if(!$result || trim($result) == "") {
    return trim($default);
  } else {
    return trim($result);
  }
}


/**
 * Return the custom excerpt or generate one from the post content
 */
function get_text_excerpt() {
  if (has_excerpt()) {
    return get_the_excerpt();
  } else {
    return wp_trim_words(get_the_content(), 55, '');
  }
}


/**
 * Get the URL of the featured image attached to the current post
 */
function get_featured_image_url($size = 'full') {
  global $post;

  $img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $size);

  if(count($img))
    return $img[0];

  return false;
}

function wm_get_featured_category() {
    $featured = get_term_by('name', 'Featured', 'category');
    return $featured;
}

/**
 * Remove Featured (and direct children) category links from the
 * category link list that displays when viewing a post
 */
function wm_remove_featured_categories($categories) {
    $featured = wm_get_featured_category();
    if(!$featured) return $categories;

    $id = $featured->term_id;

    foreach($categories as $idx => $term) {
        $term_and_parent = array($term->term_id, $term->parent);
        if(in_array($id, $term_and_parent)) {
            unset($categories[$idx]);
        }
    }

    return $categories;
}
add_filter('the_category_list', 'wm_remove_featured_categories');


/**
 * Allow iframes in body content
 */
function wm_allow_iframes($allowedposttags) {
  if (!current_user_can('publish_posts')) {
    return $allowedposttags;
  }

  $allowedposttags['iframe'] = array(
    'align' => true,
    'width' => true,
    'height' => true,
    'frameborder' => true,
    'name' => true,
    'src' => true,
    'id' => true,
    'class' => true,
    'style' => true,
    'scrolling' => true,
    'marginwidth' => true,
    'marginheight' => true,
  );

  return $allowedposttags;
}
add_filter('wp_kses_allowed_html', 'wm_allow_iframes', 1, 1);


/**
 * Prevent direct access to Featured category (and direct children) archives
 * by redirecting to the homepage
 */
function wm_block_direct_access_to_featured_categories() {
  $queried_object = get_queried_object();
  if(!$queried_object || !get_class($queried_object) == 'WP_Term') return;

  $featured = get_term_by('name', 'Featured', 'category');

  $term_and_parent = array(
    $queried_object->term_id,
    $queried_object->parent
  );

  if(in_array($featured->term_id, $term_and_parent)) {
    wp_redirect(home_url());
    exit;
  }
}
add_filter('template_redirect', 'wm_block_direct_access_to_featured_categories');

/**
 * Returns the custom display_head field with the following cascading defaults:
 * display_head > facebook_title > twitter_title > post_title
 */
function get_the_display_head() {
  $value = get_cascading_value(['display_head', 'facebook_title', 'twitter_title']);

  if(!empty($value)) return $value;

  return get_the_title();
}

/**
 * Prints the display_head
 */
function the_display_head() {
  echo get_the_display_head();
}


/**
 * Search for a non-null/non-empty value in an array of custom field names.
 * Return the first match or an empty string.
 */
function get_cascading_value($fields = array()) {
  foreach($fields as $field) {
    $value = wm_get_field($field);
    if(!empty($value)) return $value;
  }

  return '';
}

function get_category_class() {
    $category = get_top_level_category();
    return str_replace(' ', '-', strtolower($category->name));
}

/**
 * Returns the magazine issue or top-level category for the current post
 */
function get_top_level_category() {
    $main = get_main_category();

    // return the parent term if there is one
    if($main->parent) return get_term($main->parent);

    // return the main category if not
    return $main;
}

function wp_get_post_age_in_days() {
	 $age = date('U') - get_post_time('U');
	 return $age / 86400;
}

/**
 * Returns the magazine issue, child category, or top-level category for the
 * current post in that order.
 */
function get_main_category() {
    // If the post has an issue, set the term name to Magazine and return
    // the term object
    $issues = wp_get_object_terms(get_the_id(), 'issues');
    if(!empty($issues)) {
        $issues[0]->name = 'Magazine';
        return $issues[0];
    }

    $categories = get_the_category();
    $featured = wm_get_featured_category();

    // build array of non-featured categories
    $cats = [];
    foreach($categories as $category) {
        $term_and_parent = array($category->term_id, $category->parent);
        if(!in_array($featured->term_id, $term_and_parent)) {
            $cats[] = $category;
        }
    }

    // return null if there are none
    if(empty($cats)) return null;

    // return a sub-category
    foreach($cats as $category) {
        if($category->parent) return $category;
    }

    // return the last category
    return $category;
}