<?php

/**
 * Get the value of the custom option that stores the current issue
 * taxonomy term as set in the admin custom settings
 */
function wm_current_issue() {
    return get_option('wm_current_issue');
}


/**
 * Define the custom Issues taxonomy
 */
function wm_issue_taxonomy() {
    register_taxonomy(
        'issues',
        'post',
        array(
            'label' => __( 'Issues' ),
            'labels' => array(
                'name' => 'Issues',
                'singular_name' => 'Issue',
                'add_new_item' => 'Add New Issue',
                'edit_item' => 'Edit Issue',
                'new_item' => 'New Issue',
                'view_item' => 'View Issue'
            ),
            'rewrite' => array( 'slug' => 'magazine' ),
            'hierarchical' => true
        )
    );
}
add_action('init', 'wm_issue_taxonomy');


/**
* Add Issue column to the admin post listing
*/
function wm_add_issue_column($columns) {
    $new = array();
    foreach($columns as $key => $title) {
        if ($key=='tags') {
            $new['issue'] = 'Issue';
        } else {
            $new[$key] = $title;
        }
    }
    return $new;
}
add_filter('manage_posts_columns', 'wm_add_issue_column');


/**
* Display the post's issue in the issue column
*/
function wm_display_issue_column($column) {
  global $post;
  switch($column) {
    case 'issue':
    $terms = get_the_term_list( $post->post_id, 'issues', '', ',', '' );
    if(is_string($terms)) {
      echo $terms;
      break;
    }
  }
}
add_action( 'manage_posts_custom_column' , 'wm_display_issue_column');


/**
* Change permalink to magazine/issue-date/title if issue is set
*/
/* TODO: Update functions so as to not use Enhanced Permalinks plugin anymore */
function wm_issues_set_permalink($id, $post, $_stuff = null) {
  if (function_exists(custom_permalinks_original_post_link)){
    $original = custom_permalinks_original_post_link($id);
  } else {
    $original = '';
  }

  $issue = wp_get_object_terms($id, 'issues');
  if(empty($issue)) return;

  $date_slug = date('Y/m/d', strtotime($post->post_date));

  $custom_permalink = str_replace(
    $date_slug,
    "magazine/{$issue[0]->slug}",
    $original
  );

  update_post_meta($id, 'custom_permalink', $custom_permalink);
}
add_filter('save_post', 'wm_issues_set_permalink', 99, 3);
