<?php

/**
 * Define the People post type
 */
function wm_people_post_type() {
    register_post_type(
        'people',
        array(
            'label' => 'People',
            'labels' => array(
                'name' => 'People',
                'singular_name' => 'Person',
                'add_new_item' => 'Add New Person',
                'edit_item' => 'Edit Person',
                'new_item' => 'New Person',
                'view_item' => 'View Person',
             ),
            'public' => true
        )
    );
}
add_action('init', 'wm_people_post_type');


/**
 * Generate the full name post title and name for people by joining the
 * first name and last name custom fields.
 */
function wm_people_set_title($id, $post, $junk) {
  if($post->post_type != 'people') return;

  remove_action('save_post', 'wm_people_set_title', 99, 3);

  $first_name = get_field('first_name');
  $last_name = get_field('last_name');

  $args = array('ID' => $id, 'post_title' => "{$first_name} {$last_name}");

  if(is_numeric($post->post_name))
    $args['post_name'] = sanitize_title("{$first_name}-{$last_name}");

  wp_update_post($args);

  add_action('save_post', 'wm_people_set_title', 99, 3);
}
add_action('save_post', 'wm_people_set_title', 99, 3);