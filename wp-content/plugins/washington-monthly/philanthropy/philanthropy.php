<?php

function recent_philanthropy_posts_url() {
  return "http://philanthropy.washingtonmonthly.com/api/get_recent_posts?post_type=portfolio_page&count=2";
}

function get_recent_philanthropy_posts() {
  $posts = array();

  try {
    $cache = new Cache('philanthropy-homepage', 3600);

    if($cache->expired()) {
      $json = file_get_contents(recent_philanthropy_posts_url());
      $cache->cacheContent($json);
    }

    if($cache->hasContent()) $json = $cache->loadContent();
    $json = json_decode($json);
    $posts = $json->posts;
  } catch(Exception $e) { }

  return $posts;
}