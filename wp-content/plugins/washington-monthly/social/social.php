<?php

/**
 * Include metatags for facebook and twitter
 */
function wm_head_tags() {
  global $post;
  setup_postdata($post);

  wm_add_facebook_meta_tags();
  wm_add_twitter_meta_tags();

  wp_reset_postdata();
}
add_filter('wp_head', 'wm_head_tags', 9999);

/**
 * Include meta tags for facebook
 */
function wm_add_facebook_meta_tags() {
  global $post;
  $title = wm_get_field('facebook_title', get_the_title());
  $desc  = wm_get_field('facebook_description', get_text_excerpt());
  $desc = str_replace('"', "'", $desc);

  $meta = [
    'og:locale' => 'en_US',
    'og:site_name' => 'Washington Monthly',
    'og:title' => wm_trim($title),
    'og:url' => get_the_permalink(),
    'og:description' => wm_trim($desc),
    'og:updated_time' => get_the_modified_date('c')
  ];

  if($field = get_field('facebook_image')) {
    $meta['og:image'] = $field['url'];
  }
  else if($url = get_featured_image_url()) {
    $meta['og:image'] = $url;
  }

  if (get_post_type() == 'post') {
    $meta['og:type'] = 'article';
    $meta['article:published_time'] = get_the_date('c');
    $meta['article:modified_time'] = get_the_modified_date('c');
  }

  foreach($meta as $attr => $value) { meta_tag($attr, $value, 'property'); }
}


/**
 * Include metatags for twitter
 */
function wm_add_twitter_meta_tags() {
  $title = wm_get_field('twitter_title', get_the_title());
  $desc = wm_get_field('twitter_description', get_text_excerpt());
  $desc = str_replace('"', "'", $desc);

  $meta = [
    'twitter:title' => wm_trim($title),
    'twitter:url' => get_the_permalink(),
    'twitter:description' => wm_trim($desc),
  ];

  if($field = get_field('twitter_image')) {
    $meta['twitter:image:src'] = $field['url'];
    $meta['twitter:card'] = 'summary_large_image';
  }
  else if($url = get_featured_image_url()) {
    $meta['twitter:image:src'] = $url;
    $meta['twitter:card'] = 'summary_large_image';
  }

  foreach($meta as $attr => $value) { meta_tag($attr, $value, 'name'); }
}
