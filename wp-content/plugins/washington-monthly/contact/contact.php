<?php

/**
 * Override the to-address for contact form submission
 * subjects related to services
 */
function wm_set_contact_form_recipient($contact_form) {
  $submission = WPCF7_Submission::get_instance();
  $subject = $submission->get_posted_data('your-subject');

  $service_email_subjects = array('Advertising Inquiries', 'Other');

  if(in_array($subject, $service_email_subjects)) {
    $mail = $contact_form->prop('mail');
    $mail['recipient'] = 'services@washingtonmonthly.com';
    $contact_form->set_properties(array('mail' => $mail));
  }
}
add_action('wpcf7_before_send_mail', 'wm_set_contact_form_recipient');