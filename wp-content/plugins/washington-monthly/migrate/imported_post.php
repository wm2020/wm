<?php

class ImportedPost {
  function alreadyImported() {
    return is_numeric($this->id);
  }

  function loadExistingWordpressId() {
    $args = array(
      'post_type' => $this->postType,
      'meta_key' => 'mt_id',
      'meta_value' => $this->mt_id,
      'limit' => 1
    );



    $query = new WP_Query($args);
    $posts = $query->get_posts();


    $this->id = null;
    foreach($posts as $post) {

      $this->id = $post->ID;
      break;
    }
  }

  function create() {
    $post = $this->postArray();

    $id = wp_insert_post($post, true);
    if(!is_numeric($id)) return false;

    $this->id = $id;

    $this->afterSave();

    return true;
  }

  function update() {
    $post = $this->postArray();

    $id = wp_update_post($post, true);
    if(!is_numeric($id)) return false;

    $this->afterSave();

    return true;
  }
}