<?php

class Person extends ImportedPost {
  var $postType = 'people';

  public static function buildFromObject($obj) {
    $person = new Person();

    $name = trim($obj->author_nickname);
    if(empty($name)) $name = trim($obj->author_name);
    if(empty($name)) return null;

    $person->mt_id = $obj->author_id;
    $person->splitName($name);
    $person->loadExistingWordpressId();
    $person->loadShortBio($obj->author_name);

    return $person;
  }

  function splitName($name) {
    list($first, $last) = preg_split('/\s+/', $name, 2);

    $this->first_name = trim($first);
    $this->last_name = trim($last);
    $this->full_name = trim("{$this->first_name} {$this->last_name}");
  }

  function loadShortBio($author_name) {
      $this->short_bio = '';

      $file = "../bios/{$author_name}.inc";

      if(file_exists($file))
          $this->short_bio = $this->full_name . ' ' . file_get_contents($file);
      else
          $this->short_bio = '';
  }

  function postArray() {
    $post = array(
      'post_type' => 'people',
      'post_title' => $this->full_name,
      'post_status' => 'publish',
      'post_author' => 2,
    );

    if($this->alreadyImported()) {
      $post['ID'] = $this->id;
    }

    return $post;
  }

  function afterSave() {
    update_post_meta($this->id, 'first_name', $this->first_name);
    update_post_meta($this->id, 'last_name', $this->last_name);
    update_post_meta($this->id, 'mt_id', $this->mt_id);

    if(!empty($this->short_bio)) {
      update_post_meta($this->id, 'short_bio', $this->short_bio);
    }
  }
}