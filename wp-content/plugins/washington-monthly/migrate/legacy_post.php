<?php

class LegacyPost extends ImportedPost {
  var $postType = 'post';

  public static function buildFromObject($obj) {
    $post = new LegacyPost();

    $post->categories = CategoryMapping::get($obj->entry_blog_id);
    if(is_null($post->categories)) return; // skip without valid category

    $post->mt_id = $obj->entry_id;
    $post->issue = IssueMapping::get($obj->entry_keywords);
    $post->title = $obj->entry_title;
    $post->excerpt = $obj->entry_excerpt;
    $post->date = $obj->entry_created_on;
    $post->author = find_person_by_legacy_id($obj->entry_author_id);
    $post->body = trim($obj->entry_text . "\n\n" . $obj->entry_text_more);
    $post->loadExistingWordpressId();

    return $post;
  }

  function postArray() {
    $post = array(
      'post_type' => 'post',
      'post_status' => 'publish',
      'post_author' => 2,
      'post_title' => Sanitizer::clean($this->title),
      'post_excerpt' => Sanitizer::clean($this->excerpt),
      'post_content' => Sanitizer::clean($this->body),
      'post_date' => $this->date,
      'post_category' => $this->categories
    );

    $post['post_content'] = Getty::process($post['post_content']);

    if($this->alreadyImported()) {
      $post['ID'] = $this->id;
    }

    return $post;
  }

  function afterSave() {
    update_post_meta($this->id, 'mt_id', $this->mt_id);
    update_post_meta($this->id, 'author', [$this->author]);

    if($this->issue) {
      wp_set_object_terms($this->id, $this->issue, 'issues');
      wm_issues_set_permalink($this->id, get_post($this->id));
    }
  }
}