<?php

class PeopleMigrator extends Migrator {
  function querySql() {
    return "select * from mt_author";
  }

  function beforeProcess() {
    remove_action('save_post', 'wm_people_set_title', 99, 3);
  }

  function afterProcess() {
    add_action('save_post', 'wm_people_set_title', 99, 3);
  }

  function buildFromObject($object) {
    return Person::buildFromObject($object);
  }
}
