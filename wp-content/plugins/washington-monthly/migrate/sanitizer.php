<?php

class Sanitizer {
  public static function clean($string) {
    return mb_convert_encoding($string, 'utf-8', 'utf-8');
  }
}