<?php

require_once("sanitizer.php");
require_once("getty.php");
require_once("migrator.php");
require_once("imported_post.php");
require_once("issue.php");
require_once("issue_migrator.php");
require_once("person.php");
require_once("people_migrator.php");
require_once("legacy_post.php");
require_once("post_migrator.php");
require_once("issue_mapping.php");
require_once("category_mapping.php");

/**
 * Keep static array of people posts as they are found by ID. This is used by
 * the post importer when searching for and assigning authors.
 */
function find_person_by_legacy_id($id) {
  static $people = [];

  if(!isset($people[$id])) {
    $args = array(
      'post_type' => 'people',
      'meta_key' => 'mt_id',
      'meta_value' => $id
    );

    $query = new WP_Query($args);

    if(!$query->have_posts()) {
      $people[$id] = null;
    } else {
      $query->the_post();
      $people[$id] = get_the_id();
      wp_reset_postdata();
    }
  }

  return $people[$id];
}

/**
 * 301 Redirect legacy URLs to their corresponding posts if found
 */
function wm_migrate_404_override() {
  global $wp_query;

  if($wp_query->is_404) {
    if($url = wm_find_imported_post_url()) {
      $wp_query->is_404=false;
      wp_redirect($url, 301);
    }
  }
}
add_filter('template_redirect', 'wm_migrate_404_override');

/**
 * Find imported legacy post by current URL
 */
function wm_find_imported_post_url() {
  $id = wm_parse_legacy_id_from_url();
  if(!$id) return null;

  $args = array(
    'post_type' => 'post',
    'meta_key' => 'mt_id',
    'meta_value' => intval($id)
  );

  $query = new WP_Query($args);
  if(!$query->have_posts()) return null;

  $query->the_post();
  $url = get_the_permalink();
  wp_reset_postdata();

  return $url;
}

/**
 * Parse 6-digit legacy ID from current URL
 */
function wm_parse_legacy_id_from_url() {
  $url = $_SERVER['REQUEST_URI'];

  preg_match('/\d{6}/', $url, $matches);

  if(count($matches)) {
    return array_pop($matches);
  }
}
