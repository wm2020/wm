<?php

class IssueMigrator extends Migrator {
  var $importOnly = true;

  function querySql() {
    return "select * from mt_entry where entry_blog_id = 8";
  }

  function buildFromObject($object) {
    return Issue::buildFromObject($object);
  }
}
