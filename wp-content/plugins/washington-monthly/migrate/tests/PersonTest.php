<?php

require_once("person.php");

class PersonTest extends PHPUnit_Framework_TestCase {
  public function testBuildFromObject() {
    $obj = new StdClass();
    $obj->author_nickname = "Joe Smith";

    $person = Person::buildFromObject($obj);

    $this->assertEquals("Joe", $person->first_name);
    $this->assertEquals("Smith", $person->last_name);
    $this->assertEquals("Joe Smith", $person->full_name);
  }

  public function testSplitNameWithOneWord() {
    $person = new Person();
    $person->splitName("Joe");

    $this->assertEquals("Joe", $person->first_name);
    $this->assertEquals("", $person->last_name);
    $this->assertEquals("Joe", $person->full_name);
  }

  public function testSplitNameWithTwoWords() {
    $person = new Person();
    $person->splitName("Joe Smith");

    $this->assertEquals("Joe", $person->first_name);
    $this->assertEquals("Smith", $person->last_name);
    $this->assertEquals("Joe Smith", $person->full_name);
  }

  public function testSplitNameWithThreeWords() {
    $person = new Person();
    $person->splitName("Joe Smith Jr.");

    $this->assertEquals("Joe", $person->first_name);
    $this->assertEquals("Smith Jr.", $person->last_name);
    $this->assertEquals("Joe Smith Jr.", $person->full_name);
  }
}