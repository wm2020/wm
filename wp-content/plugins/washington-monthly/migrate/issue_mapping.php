<?php

class IssueMapping {
  private static $instance;

  public static function getInstance() {
    if(null === static::$instance) static::$instance = new static();

    return static::$instance;
  }

  public static function get($key) {
    $instance = static::getInstance();

    if(!isset($instance->map[$key])) return null;

    return $instance->map[$key];
  }

  var $map = array(
    // entry_keywords => issue term name
    'March/April 2011' => 'March/April 2011',
    'May/June 2011' => 'May/June 2011',
    'July/August 2011' => 'July/Aug 2011',
    'September/October 2011' => 'Sept/Oct 2011',
    'November/December 2011' => 'Nov/Dec 2011',
    'November/ December 2011' => 'Nov/Dec 2011',
    'January/February 2012' => 'Jan/Feb 2012',
    'January/ February 2012' => 'Jan/Feb 2012',
    'March/ April 2012' => 'March/April 2012',
    'March/April 2012' => 'March/April 2012',
    'May/June 2012' => 'May/June 2012',
    'September/October 2012' => 'Sept/Oct 2012',
    'July/August 2012' => 'July/August 2012',
    'September/ October 2012' => 'Sept/Oct 2012',
    'November/December 2012' => 'Nov/Dec 2012',
    'November/ December 2012' => 'Nov/Dec 2012',
    'January/ February 2013' => 'Jan/Feb 2013',
    'January/February 2013' => 'Jan/Feb 2013',
    'March/ April 2013' => 'March/April 2013',
    'May/ June 2013' => 'May/June 2013',
    'July/ August 2013' => 'July/August 2013',
    'September/ October 2013' => 'Sept/Oct 2013',
    'November/ December 2013' => 'Nov/Dec 2013',
    'January/ February 2014' => 'Jan/Feb 2014',
    'February 25, 2014' => 'Jan/Feb 2014',
    'March/ April/ May 2014' => 'March/April/May 2014',
    'March 24, 2014' => 'March/April/May 2014',
    'June/July/August 2014' => 'June/July/Aug 2014',
    'September/October 2014' => 'Sept/Oct 2014',
    'November/December 2014' => 'Nov/Dec 2014',
    'January/February 2015' => 'Jan/Feb 2015',
    'March/April/May 2015' => 'Mar/Apr/May 2015',
    'June/July/August 2015' => 'June/July/Aug 2015',
    'September/October 2015' => 'Sept/Oct 2015',
    'November/December 2015' => 'Nov/Dec 2015',
    'January/February 2016' => 'Jan/Feb 2016',
    'March/April/May 2016' => 'Mar/Apr/May 2016',
  );
}