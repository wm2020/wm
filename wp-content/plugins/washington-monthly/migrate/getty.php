<?php

class Getty {
  var $width = 594;
  var $height = 396;

  function __construct($markup) {
    $this->markup = $markup;
  }

  function markup() {
    return $this->markup;
  }

  public static function process($markup) {
    if(empty($markup)) return "";

    $getty = new static($markup);
    $getty->adjustFrameWidths();
    return $getty->markup();
  }

  function adjustFrameWidths() {
    $doc = new DOMDocument();
    libxml_use_internal_errors(true);

    $doc->loadHTML(
      mb_convert_encoding($this->markup, 'HTML-ENTITIES', 'UTF-8'),
      LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD
    );

    $xpath = new DOMXPath($doc);

    // Change hardcoded max-width of container
    $elements = $xpath->query('//div[contains(@class,"getty")]');
    foreach($elements as $element) {
      $style = preg_replace(
        '/max-width:470px/',
        "max-width:{$this->width}px",
        $element->getAttribute('style')
      );

      $element->setAttribute('style', $style);
    }

    // Change hardcoded width and height of iframes
    $frames = $xpath->query('//div[contains(@class,"getty")]//iframe');
    foreach($frames as $frame) {
      $frame->setAttribute('width', $this->width);
      $frame->setAttribute('height', $this->height);

      $style = $frame->getAttribute('style');
      $style = preg_replace('/height:100\%/', "height:{$this->height}px", $style);
      $frame->setAttribute('style', $style);
    }

    $this->markup = $doc->saveHTML();

    return $this;
  }
}