<?php

class Issue {
  function alreadyImported() {
    return term_exists($this->title, 'issues');
  }

  public static function buildFromObject($obj) {
    $issue = new Issue();
    $issue->mt_id = $obj->entry_id;
    $issue->title = $obj->entry_title;
    $issue->date = $obj->entry_created_on;
    return $issue;
  }

  function update() {
    return false;
  }

  function create() {
    $term = wp_insert_term($this->title, 'issues');
    $this->id = $term['term_id'];
    update_term_meta($this->id, 'mt_id', $this->mt_id);
    return true;
  }
}