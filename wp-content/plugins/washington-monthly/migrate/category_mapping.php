<?php

class CategoryMapping {
  private static $instance;

  public static function getInstance() {
    if(null === static::$instance) static::$instance = new static();

    return static::$instance;
  }

  public static function get($key) {
    $instance = static::getInstance();

    if(!isset($instance->map[$key])) return null;

    return $instance->map[$key];
  }

  var $map = array(
    1  => [3, 4],  // Political Animal
    4  => [3, 4],  // 2011 Political Animal
    10 => [3, 4],  // 2012 Political Animal
    11 => [5, 7],  // The Grade
    3  => [5, 6],  // College Guide
    6  => [16],    // Magazine
    5  => [3],     // Ten Miles Square
    12 => [10, 9], // Republic 3.0
    8  => null,    // ToC Creator
    9  => null,    // Test Blog Simple
    7  => null,     // WaMo Dynamic
    2  => null,     // How's Europe Doing?
  );
}