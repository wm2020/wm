<?php
class Migrator {
  var $limit = null;
  var $offset = null;
  var $importOnly = false;

  function __construct($config = array()) {
    $this->db = new mysqli("127.0.0.1", "root", "root", "wamo_mt");

    foreach($config as $key => $value) {
      $this->{$key} = $value;
    }
  }

  function limitAndOffset() {
    $query = '';

    if(is_numeric($this->limit)) {
      $query .= " LIMIT {$this->limit}";

      if(is_numeric($this->offset)) {
        $query .= " OFFSET {$this->offset}";
      }
    }

    return $query;
  }

  function fullQuerySql() {
    return $this->querySql() . $this->limitAndOffset();
  }

  function query() {
    return $this->db->query($this->fullQuerySql());
  }

  function process() {
    $this->beforeProcess();

    $counts = [
      'created' => 0,
      'updated' => 0,
      'created_error' => 0,
      'updated_error' => 0,
      'total' => 0,
      'skipped' => 0];

    $query = $this->query();

    while($row = $query->fetch_object()) {
      $object = $this->buildFromObject($row);

      if(!$object) {
        $counts['skipped']++;
        continue;
      }

      if($object->alreadyImported()) {
        if($this->importOnly) {
          $counts['skipped']++;
          continue;
        }
        elseif($result = $object->update()) {
          $counts['updated']++;
        } else {
          $counts['updated_error']++;
        }
      } else {
        if($result = $object->create()) {
          $counts['created']++;
        } else {
          $counts['created_error']++;
        }
      }
    }

    $this->afterProcess();

    return $counts;
  }

  function beforeProcess() { return true; }
  function afterProcess() { return true; }
  function buildFromObject($object) { return null; }
  function querySql() { return ""; }
}
