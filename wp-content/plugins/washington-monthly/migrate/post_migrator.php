<?php

class PostMigrator extends Migrator {
  function querySql() {
    return "SELECT * FROM mt_entry WHERE entry_id >= 60512 AND entry_status = 2 AND " .
                    "entry_blog_id NOT IN (2,7,8,9) ORDER BY entry_id DESC";
  }

  function buildFromObject($object) {
    return LegacyPost::buildFromObject($object);
  }

  function beforeProcess() {
    remove_filter('content_save_pre', 'wp_filter_post_kses');
    remove_filter('content_filtered_save_pre', 'wp_filter_post_kses');
  }

  function afterProcess() {
    add_filter('content_save_pre', 'wp_filter_post_kses');
    add_filter('content_filtered_save_pre', 'wp_filter_post_kses');
  }
}
