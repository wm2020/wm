#!/usr/bin/php

<?php
ini_set('memory_limit','512M');
ini_set('display_errors', 1);
error_reporting(E_ALL);

function print_results($results) {
  $total = 0;
  foreach($results as $label => $count) {
    $total += $count;
    echo "{$label}: {$count}\n";
  }

  echo "total: {$total}\n\n";
}

//setup global $_SERVER variables to keep WP from trying to redirect
$_SERVER = array(
  "HTTP_HOST" => "localhost.com",
  "SERVER_NAME" => "http://localhost.com",
  "REQUEST_URI" => "/",
  "REQUEST_METHOD" => "GET",
  "SCRIPT_NAME" => "migrate.php",
  "PHP_SELF" => "migrate.php",
);

//require the WP bootstrap
require_once("../../../../../wp-load.php");


/*
$migrator = new IssueMigrator();
echo "Importing issues...\n";
echo $migrator->fullQuerySql() . "\n";
print_results($migrator->process());
die; /* */

//*
$migrator = new PeopleMigrator([
    'limit' => null,
    'offset' => null,
    'importOnly' => false,
  ]);
echo "Importing people...\n";
echo $migrator->fullQuerySql() . "\n";
print_results($migrator->process());
die; /* */

$migrator = new PostMigrator([
    'limit' => 100,
    'offset' => 54525,
    'importOnly' => false
  ]);
echo "Importing posts...\n";
echo $migrator->fullQuerySql() . "\n";
print_results($migrator->process());
