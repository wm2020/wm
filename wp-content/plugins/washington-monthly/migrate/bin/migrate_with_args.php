#!/usr/bin/php

<?php
ini_set('memory_limit','512M');
ini_set('display_errors', 1);
error_reporting(E_ALL);

$limit = $argv[1];
$offset = $argv[2];

function print_results($results) {
  $total = 0;
  foreach($results as $label => $count) {
    $total += $count;
    echo "{$label}: {$count}\n";
  }

  echo "{$total}";
}

//setup global $_SERVER variables to keep WP from trying to redirect
$_SERVER = array(
  "HTTP_HOST" => "localhost.com",
  "SERVER_NAME" => "http://localhost.com",
  "REQUEST_URI" => "/",
  "REQUEST_METHOD" => "GET",
  "SCRIPT_NAME" => "migrate.php",
  "PHP_SELF" => "migrate.php",
);

//require the WP bootstrap
require_once("../../../../../wp-load.php");

$migrator = new PostMigrator([
    'limit' => $limit,
    'offset' => $offset,
    'importOnly' => false
  ]);
echo "Importing posts...\n";
echo $migrator->fullQuerySql() . "\n";
print_results($migrator->process());
