limit = 25
offset = 56059

loop do
  start_time = Time.now.to_f

  command = "php migrate_with_args.php #{limit} #{offset}"
  puts command

  processed_count = `#{command}`
  processed_count = processed_count.strip.to_i

  end_time = Time.now.to_f

  puts "time: #{end_time - start_time}"

  break if processed_count <= 0

  offset = offset + processed_count.to_i
end
