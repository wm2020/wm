<?php
namespace NinjaTablesPro\DataProviders;

class RawSqlProvider
{
    public function boot()
    {
        add_filter('ninja_tables_get_table_raw_sql', array($this, 'getTableSettings'));
        add_filter('ninja_tables_get_table_data_raw_sql', array($this, 'getTableData'), 10, 4);
        add_filter('ninja_tables_fetching_table_rows_raw_sql', array($this, 'data'), 10, 5);
        add_action('wp_ajax_ninja_tables_save_raw_sql_table', array($this, 'createTable'));
        add_action('wp_ajax_ninja_table_raw_sql_update_sql', array($this, 'updateSQL'));
        add_filter('ninja_table_activated_features', function ($features) {
            $features['raw_sql_query'] = true;
            return $features;
        });
    }

    public function createTable()
    {
        if(!current_user_can(ninja_table_admin_role())) {
            return;
        }
        $sql = wp_unslash($_REQUEST['sql']);
        // Validate the SQL and see if we have any data or not
        $this->validateSql($sql);

        // Validate Title
        $messages = array();
        if (empty($_REQUEST['post_title'])) {
            $messages[] = __('The title field is required.', 'ninja-tables');
            wp_send_json_error(array('message' =>   $messages), 422);
            wp_die();
        }


        // Create The Table now
        $tableId = $this->saveTable();
        update_post_meta($tableId, '_ninja_table_raw_sql_query', $sql);
        update_post_meta($tableId, '_ninja_tables_data_provider', 'raw_sql');
        $this->createColumns($tableId, $sql);

        wp_send_json_success(
            array('table_id' => $tableId)
        );

    }

    public function updateSQL()
    {
        if(!current_user_can(ninja_table_admin_role())) {
            return;
        }

        $tableId = absint($_REQUEST['table_id']);
        $sql = wp_unslash($_REQUEST['sql']);
        $this->validateSql($sql);

        update_post_meta($tableId, '_ninja_table_raw_sql_query', $sql);

        $this->updateTableColumn($tableId, $sql);

        wp_send_json_success(array(
            'message' => 'SQL successfully updated'
        ), 200);
    }

    public function validateSql($sql)
    {
        $low_sql = strtolower($sql);
        $hasBadKeyWord = strpos($low_sql, 'delete ') !== false || strpos($low_sql, 'update ') !== false || strpos($low_sql, 'insert ') !== false;
        $hasSelect = strpos($low_sql, 'select ') !== false;
        if($hasBadKeyWord || !$hasSelect) {
            wp_send_json_error(array(
                'message' => array('SQL is not valid'),
                'error' => 'We could not validate your provided SQL. Please try another SQL.'
            ), 423);
        }

        global $wpdb;
        ob_start();
        $results = $wpdb->get_results($sql);
        $errors = ob_get_clean();
        if($errors) {
            wp_send_json_error(array(
                'message' => array('SQL is not valid'),
                'error' => $errors
            ), 423);
            wp_die();
        } else if(!$results) {
            wp_send_json_error(array(
                'message' => array('No data fetched from the provided the SQL. Please revise your SQL')
            ), 423);
            wp_die();
        }
        return true;
    }

    public function createColumns($tableId, $sql)
    {
        // We have to create the columns and additional settings now
        global $wpdb;
        $row = $wpdb->get_row($sql);
        $fields = array();
        foreach ($row as $row_key => $row_value) {
            $fields[] = $row_key;
        }

        $columns = array();

        foreach ($fields as $key => $column) {
            $columns[] = array(
                'name' => $column,
                'key' => $column,
                'breakpoints' => null,
                'data_type' => 'text',
                'dateFormat' => null,
                'header_html_content' => null,
                'enable_html_content' => false,
                'contentAlign' => null,
                'textAlign' => null,
                'original_name' => $column,
                'original_key' => $column
            );
        }

        $tableSettings = ninja_table_get_table_settings($tableId, 'admin');
        update_post_meta($tableId, '_ninja_table_settings', $tableSettings);
        update_post_meta($tableId, '_ninja_table_columns', $columns);
    }

    public function updateTableColumn($tableId, $sql)
    {
        // We have to create the columns and additional settings now
        global $wpdb;
        $row = $wpdb->get_row($sql);
        $columns = array();
        foreach ($row as $row_key => $row_value) {
            $columns[] = $row_key;
        }

        $existingColumns = get_post_meta($tableId, '_ninja_table_columns', true);
        $exisitingColumnKeys = array();
        foreach ($existingColumns as $column) {
            $exisitingColumnKeys[$column['original_key']] = $column;
        }
        $formattedColumns = array();
        foreach ($columns as $key => $column) {
            if(isset($exisitingColumnKeys[$column])) {
                $exisitingColumnKeys[$column]['original_key'] = $column;
                $exisitingColumnKeys[$column]['key'] = $column;
                $exisitingColumnKeys[$column]['original_name'] = $column;
                $formattedColumns[] = $exisitingColumnKeys[$column];
            } else {
                $formattedColumns[$key] = array(
                    'name' => $column,
                    'key' => $column,
                    'breakpoints' => null,
                    'data_type' => 'text',
                    'dateFormat' => null,
                    'header_html_content' => null,
                    'enable_html_content' => false,
                    'contentAlign' => null,
                    'textAlign' => null,
                    'original_name' => $column,
                    'original_key' => $column
                );
            }
        }
        update_post_meta($tableId, '_ninja_table_columns', $formattedColumns);
    }

    public function getTableSettings($table)
    {
        $table->isEditable = false;
        $table->dataSourceType = get_post_meta($table->ID, '_ninja_tables_data_provider', true);
        $table->isEditableMessage = ' to edit your SQL query please click here';
        $table->isExportable = true;
        $table->isImportable = false;
        $table->isSortable = false;
        $table->hasCacheFeature = false;
        $table->isCreatedSortable = false;
        $table->hasExternalCachingInterval = false;
        $table->sql = get_post_meta($table->ID, '_ninja_table_raw_sql_query', true);
        return $table;
    }

    public function getTableData($data, $tableId, $perPage, $offset)
    {
        $newData = array();
        $sql = get_post_meta($tableId, '_ninja_table_raw_sql_query', true);

        global $wpdb;
        ob_start();
        $results = $wpdb->get_results($sql);
        $errors = ob_get_clean();

        if($errors) {
            $results = array();
        }

        $totalData = count($results);

        $responseData = array_slice($results, $offset, $perPage);

        foreach ( $responseData as $key => $value) {
            $newData[] = array(
                'id' => $key + 1,
                'values' => $value,
                'position' => $key + 1,
            );
        }
        return array(
            $newData,
            $totalData
        );
    }

    public function data($data, $tableId, $defaultSorting, $limitEntries = false, $skip = false)
    {
        $sql = get_post_meta($tableId, '_ninja_table_raw_sql_query', true);
        global $wpdb;
        ob_start();
        $results = $wpdb->get_results($sql, ARRAY_A);
        $errors = ob_get_clean();
        if($errors) {
            $results = array();
        }


        return $results ? $results : $data;
    }

    protected function saveTable($postId = null)
    {
        $attributes = array(
            'post_title' => sanitize_text_field($this->get($_REQUEST, 'post_title')),
            'post_content' => wp_kses_post($this->get($_REQUEST, 'post_content')),
            'post_type' => 'ninja-table',
            'post_status' => 'publish'
        );

        if (!$postId) {
            $postId = wp_insert_post($attributes);
        } else {
            $attributes['ID'] = $postId;
            wp_update_post($attributes);
        }
        return $postId;
    }

    protected function get($array, $key, $default = false)
    {
        if (isset($array[$key])) {
            return $array[$key];
        }
        return $default;
    }

    public function returnFalse() {
        return false;
    }
}
