<?php if(has_excerpt()) : ?>
    <h4 class="sub"><?php echo strip_tags(get_the_excerpt()); ?></h4><br>
<?php endif; ?>