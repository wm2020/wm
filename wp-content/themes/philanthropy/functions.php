<?php

require_once 'portfolio_list_shortcode.php';

/***  Enqueue Elated child theme stylesheet ***/

function elated_child_enqueue_style() {
	wp_register_style('philanthropy', get_stylesheet_directory_uri() . '/style.css');
	wp_enqueue_style('philanthropy');

  wp_register_script('custom', get_stylesheet_directory_uri() . '/custom.js');
  wp_enqueue_script('custom');
}

add_action( 'wp_enqueue_scripts', 'elated_child_enqueue_style', 11);

/**
 * Add custom image sizes
 */
add_image_size('portfolio-slider', 738, 400, true);
add_image_size('archive', 334, 188, true);

/**
 * Override eltd_excerpt to resolve shortcodes before stripping HTML
 */
function eltd_excerpt() {
  global $eltd_options, $eltd_word_count, $post;

  if(post_password_required()) {
    echo get_the_password_form();
  }

  //does current post has read more tag set?
  elseif(eltd_post_has_read_more()) {
    global $more;

    //override global $more variable so this can be used in blog templates
    $more = 0;
    the_content(true);
  }

  //is word count set to something different that 0?
  elseif($eltd_word_count != '0') {

    if(has_excerpt()) {
      echo '<p class="post_excerpt">'.wp_kses_post(get_the_excerpt()).'</p>';
      return;
    }

    //if word count is set and different than empty take that value, else that general option from theme options
    $eltd_word_count = isset($eltd_word_count) && $eltd_word_count !== "" ? $eltd_word_count : esc_attr($eltd_options['number_of_chars']);

    //if post excerpt field is filled take that as post excerpt, else that content of the post
    $post_excerpt = $post->post_excerpt != "" ? $post->post_excerpt : strip_tags($post->post_content);

    //start with full content
    $post_excerpt = $post->post_content;

    //handle shortcodes
    $post_excerpt = do_shortcode($post_excerpt);

    // strip html
    $post_excerpt = strip_tags($post_excerpt);

    //remove leading dots if those exists
    $clean_excerpt = strlen($post_excerpt) && strpos($post_excerpt, '...') ? strstr($post_excerpt, '...', true) : $post_excerpt;

    //if clean excerpt has text left
    if($clean_excerpt !== '') {
      //explode current excerpt to words
      $excerpt_word_array = explode (' ', $clean_excerpt);

      //cut down that array based on the number of the words option
      $excerpt_word_array = array_slice ($excerpt_word_array, 0, $eltd_word_count);

      //add exerpt postfix
      $excert_postfix		= apply_filters('eltd_excerpt_postfix', '...');

      //and finally implode words together
      $excerpt 			= implode (' ', $excerpt_word_array).$excert_postfix;

      //is excerpt different than empty string?
      if($excerpt !== '') {
	echo '<p class="post_excerpt">'.wp_kses_post($excerpt).'</p>';
      }
    }
  }
}

/**
 * Include portfolio pages in author archive
 */
function custom_post_author_archive(&$query) {
  if ($query->is_author) {
    $query->set('post_type', 'portfolio_page');
    remove_action('pre_get_posts', 'custom_post_author_archive'); // run once!
  }
}
add_action('pre_get_posts', 'custom_post_author_archive');