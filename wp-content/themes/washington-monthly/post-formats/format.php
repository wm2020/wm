<?php
  /*
    * This is the default post format.
    *
    * So basically this is a regular post. if you don't want to use post formats,
    * you can just copy ths stuff in here and replace the post format thing in
    * single.php.
    *
    * The other formats are SUPER basic so you can style them as you like.
    *
    * Again, If you want to remove post formats, just delete the post-formats
    * folder and replace the function below with the contents of the "format.php" file.
  */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

  <header class="article-header entry-header">

    <!-- CUSTOM OUTPUT: DISPLAY ISSUE LINKS ABOVE TITLE -->
    <div class="issue-header"><?php echo get_the_term_list( get_the_id(), 'issues') ?></div>

    <h1 class="single-title" itemprop="headline" rel="bookmark">
      <?php the_title() ?>
    </h1>
    <?php echo $form_fields; ?>

    <?php if(!get_field('hide_subtitle') && has_excerpt()) : ?>
    <h4 class="entry-subtitle single-subtitle"><?php echo the_excerpt() ?></h4>
    <?php endif; ?>

    <?php get_template_part('snippets/byline-author-list'); ?>
    <?php get_template_part('snippets/publication-date'); ?>
    <?php get_template_part('snippets/tag-header'); ?>

  </header> <?php // end article header ?>
  <?php $key='dropcap';
    $dropcap = get_post_meta($post->ID, $key, true);
    ?>
  <section class="entry-content cf <?php if ($dropcap == 'Yes') { echo "has-dropcap"; }?>" itemprop="articleBody">
    <?php get_template_part('snippets/post-featured-image') ?>
    <?php get_template_part('snippets/social-individual-post') ?>
    <?php
      the_content_with_ads();

      /*
        * Link Pages is used in case you have posts that are set to break into
        * multiple pages. You can remove this if you don't plan on doing that.
        *
        * Also, breaking content up into multiple pages is a horrible experience,
        * so don't do it. While there are SOME edge cases where this is useful, it's
        * mostly used for people to get more ad views. It's up to you but if you want
        * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
        *
        * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
        *
      */
      wp_link_pages( array(
        'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
        'after'       => '</div>',
        'link_before' => '<span>',
        'link_after'  => '</span>',
      ) );
    ?>
  </section> <?php // end article section ?>
  
  <?php get_template_part('snippets/ads/lockerdome') ?>
  <?php dynamic_sidebar( 'content-bottom' ); ?>
  <?php get_template_part('snippets/author-list') ?>
  <?php get_template_part('snippets/related') ?>

<div id="taboola-below-article-thumbnails"></div>
  <script type="text/javascript">
    window._taboola = window._taboola || [];
    _taboola.push({
      mode: 'thumbnails-a',
      container: 'taboola-below-article-thumbnails',
      placement: 'Below Article Thumbnails',
      target_type: 'mix'
    });
  </script>

  <?php comments_template(); ?>

</article> <?php // end article ?>
