jQuery(document).ready(function($) {

  function windowWidth() {
    if ($(window).width() < 960) {
      // $('#mega-menu-main-nav').insertBefore('#container').addClass('hide');

      function toggleClick() {
        $(window).scrollTop(0);
        // $('body').toggleClass('nav-open');
        // setTimeout(function() {
        //   $('#mega-menu-main-nav').toggleClass('hide');
        // },200);
      }

      $('.mega-menu-toggle').on('click', function(){
        toggleClick();
      });

      // $('.nav-open #content:before').on('focus', function() {
      //   $('body').removeClass('nav-open');
      // })
    } 
    // else {
    //   $('#mega-menu-main-nav').insertAfter('.mega-menu-toggle');
    // }
  }

  $(window).on('resize', function() {
    windowWidth();
  }).resize();

  // var scrollTop = $(window).scrollTop();
  // $(window).on('scroll', function() {
  //   console.log(scrollTop);
  //   if ($('body').hasClass('nav-open') && scrollTop > 1000) {
  //     toggleClick();
  //   }
  // })

  if ($('body').hasClass('nav-open')) {
    $('#mega-menu-main-nav').show();
    $(window).on('scroll', function() {
      $('#mega-menu-main-nav').show();
    })
  }
});