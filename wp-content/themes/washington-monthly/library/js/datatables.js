var $ = jQuery;

$(document).ready(function($) {
    setTimeout(function(){
        $('td, th').each(function(){
            if($(this).is(':hidden')){
                $(this).css('display','table-cell');
            }
        });
        var thHeight = 0;
        $('.DTFC_ScrollWrapper th').each(function(){
            if($(this).height() > thHeight){
                console.log($(this).height());
                thHeight = $(this).height();
                console.log(thHeight);
            }
        });
        $('.DTFC_LeftWrapper th:first-child').each(function(){
            $(this).height(thHeight);
        });
        $('.igsv-table').css('opacity',1);
        $('#table-loader').fadeOut(100);
    },500);    
});