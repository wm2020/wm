$(document).ready(function() {
    $('.ga-track').click(function() {
        handleOutboundLinkClicks(this);
    });
});

function handleOutboundLinkClicks(event) {
    ga('send', 'event', {
        eventCategory: 'Subscribe Link',
        eventAction: 'click',
        eventLabel: event.href,
        transport: 'beacon'
    });
}