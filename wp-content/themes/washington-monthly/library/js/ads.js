(function($) {
  $(window).load(function() {
    $(".ad-embed").each(function() {
      var ad_container = $(this);

      ad_container.find("script[type='template/ad']").each(function() {
        var ad_src = $(this).attr("data-src");
        $(this).remove();

        var ad_tag = document.createElement("script");
        ad_tag.type = "text/javascript";
        ad_tag.src = ad_src;

        ad_container.append(ad_tag);
      });
    });
  });

  $(".aside .ad, .sidebar .ad").each(function() {
    $(this).addClass("ad-aside");
  });

  // Hotfix for single ad violation page
  if ($(".postid-20036").length) {
    $(".postid-20036 .adsbygoogle[data-ad-client='ca-pub-9037807250275212'")
      .parent("p")
      .first()
      .remove();
  }
})(jQuery);
