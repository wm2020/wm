// (function ($, document) {

//   // Customize these variables.
//   // ----------------------------------
//   var popups = [], // Comma separated popup IDs.
//       cookie_name = 'pum-split-test', // Cookie name for the test only.
//       cookie_time = '6 hours', // Cookie timer.
//       // ------------------------------
//       // End Customizations.
//       chosen_popup = false; // Empty placeholder.

//   function random_popup() {
//       return popups[Math.floor(Math.random() * popups.length)];
//   }

//   function get_chosen_popup() {
//       var popup,
//           cookie;

//       if ($.pm_cookie === undefined) {
//         return 0;
//       }

//       if (document.cookie.indexOf(cookie_name) !== -1) {
//         return 0;
//       }

//       cookie = parseInt($.pm_cookie(cookie_name)) || false;

//       // If the cookie contains a value use it.
//       if (cookie > 0 && popups.indexOf(cookie) !== -1) {
//           popup = cookie;
//       } else if (!cookie) {
//           popup = random_popup();
//           $.pm_cookie(cookie_name, popup, cookie_time, '/');
//           PUM.open(popup);
//       }

//       return popup;
//   }

//   // Prevent non chosen popups from opening.
//   $(document).ready(function () {

//       var $this = $(this),
//           ID = $this.popmake('getSettings').id;

//       if (!chosen_popup) {
//         chosen_popup = get_chosen_popup();
//       }

//       if (popups.indexOf(ID) >= 0 || ID !== chosen_popup) {
//           $this.addClass('preventOpen');
//       } else {
//           $this.removeClass('preventOpen');
//       }
//   });
// }(jQuery, document));