//search bar functionality
jQuery(document).ready(function($) {
  $('.search').on('click', function() {
    $(this).toggleClass('focus');
    $('#s').focus();
    $('.fa').toggleClass('fa-search');
    $('.fa').toggleClass('fa-close');
  });
  $('#content').click(function() {
    $('.search').removeClass('focus');
  });
});