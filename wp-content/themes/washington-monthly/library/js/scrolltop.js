//Scroll functionality for navbar
jQuery(document).ready(function($) {
    //caches a jQuery object containing the header element
    var header = $(".header");
    var scrollHeight = $('.header').outerHeight() + $('.entry-header').outerHeight() + $('.featured-image').outerHeight();
    // console.log(scrollHeight);
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= scrollHeight) {
            header.addClass("scrolled");
        } else {
            header.removeClass("scrolled");
        }

        // var distance = $('.college-guide-embed').offset().top - header.outerHeight();
        //     $window = $(window);

        // if ( $window.scrollTop() >= distance ) {
        //     $('.college-guide-embed').addClass('fixed');
        // } else {
        //     $('.college-guide-embed').removeClass('fixed');
        // }
    });
}); //End document ready