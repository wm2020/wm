<?php
$category = get_main_category();
if(!$category) return; // skip the rest if no category is found

$cat_class = get_category_class();
?>
<div class="header-tag title--category header--<?php echo $cat_class; ?>">
  <a href="<?php echo esc_url(get_category_link($category->term_id)) ?>">
    <?php echo esc_html($category->name) ?>
  </a>
</div>
