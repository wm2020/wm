<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

	<header class="entry-header article-header">
		<h3 class="h2 entry-title">
      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

    <?php get_template_part('snippets/byline-author-list') ?>
    <?php get_template_part('snippets/publication-date') ?>
	</header>

	<section class="entry-content cf">
		<?php the_content_with_ads(500, 500) ?>
    <a href="<?php the_permalink() ?>#disqus_thread" data-disqus-identifier="<?php the_disqus_id(); ?>" class="disqus-count"></a>
    <?php get_template_part('snippets/social-individual-post') ?>
	</section>

	<footer class="article-footer">
	</footer>
</article>