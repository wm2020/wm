<?php $posts = get_recent_philanthropy_posts(); ?>
<?php if(!empty($posts)) : ?>

<div class="featured-posts philanthropy-homepage-block">
  <a href="https://philanthropy.washingtonmonthly.com" target="_blank" class="block-title">Successes of Philanthropy</a>

<?php foreach($posts as $post) : ?>
  <div class="featured-post-tile">
    <div class="featured-image">
      <a href="<?php echo $post->url; ?>" target="_blank"
         style="background-image: url(<?php echo $post->thumbnail_images->medium->url; ?>);"></a>
      <h2>
        <a href="<?php echo $post->url; ?>"><?php echo $post->title; ?></a>
      </h2>

      <div class="meta">
        <div class="author-info">
          <span class="by">by</span> <?php echo $post->author->name; ?>
        </div>

      </div>
    </div>

    <p><?php echo strstr(strip_tags($post->excerpt, '<p>'), '.', true); ?>.</p>
  </div>
<?php endforeach; ?>

</div>

<?php endif; ?>