<?php // display when adblock is present?>
<?php if (is_active_sidebar('leaderboard-abp')) : ?>
    <div class="wrap">
        <?php dynamic_sidebar('leaderboard-abp'); ?>
    </div>
<?php endif; ?>

<?php if (is_active_sidebar('leaderboard')) : ?>
  <div class="ad-leaderboard ad">      
      <?php dynamic_sidebar('leaderboard'); ?>
  </div>
<?php endif; ?>