<?php
$page_var = 'post_page';
$page = (isset($_GET[$page_var])) ? $_GET[$page_var] : 1;

$author_id   = get_the_id();
$author_name = get_the_title();

$custom_query = query_posts_by_author($author_id, $page);
if(!$custom_query->have_posts()) return;

// echo "<h3>Articles by {$author_name}</h3>";

?>
<hr />
<?php

$original_post = $post;

while ($custom_query->have_posts()) {
  $custom_query->the_post();
  get_template_part('snippets/post-listing-item');
}

echo "<br>";

echo "<nav class='pagination'>";
echo paginate_links(array(
  'format' => '?'. $page_var .'=%#%',
  'current' => max(1, $custom_query->get('paged')),
  'total' => $custom_query->max_num_pages,
  'prev_text'    => '&larr;',
  'next_text'    => '&rarr;',
  'type'         => 'list',
  'end_size'     => 3,
  'mid_size'     => 3
));
echo "</nav>";

$post = $original_post;
