<?php
$authors = get_field('author');
if (empty($authors)) {
    return;
}

$count = count($authors);

  echo "<div class='author-info'><span class='by'>by</span> ";

$original_post = $post;

foreach ($authors as $idx => $author) {
    $post = $author;

    if ($idx > 0 && $count > 2) {
        echo ", ";
    }
    if ($count > 1 && $idx == $count - 1) {
        echo " and ";
    }

    get_template_part('snippets/byline-author-list-item');
}

echo "</div>";

$post = $original_post;
