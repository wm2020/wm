<?php
//$categories = get_option('wm_homepage_categories');

$categories = [
  16, // Magazine,
  2805, //College Guide  
  3, // Politics
  5, // Education
  2804, // Monopolized Economy
  13, // Books
  8, // Economy
  11 // Healthcare
];


if(empty($categories)) return;
?>
<div class="homepage-category-blocks" style="clear:both;">
  <?php
  foreach($categories as $cat) {
    $category = get_category($cat);
    $category_url = get_category_link($category->term_id);

    $offset = 0;
    if($category->term_id == 4) {
      $offset = get_pa_offset();
    }

    $highlighted_posts = query_highlighted_category_posts(
      $category->term_id, true, $offset);
      
    $sticky_post = query_sticky_category_post($category->term_id, $offset);

    if($highlighted_posts->have_posts()) {
      include(locate_template('snippets/highlighted-category-block.php'));
    }

    if($category->term_id == 16) {
      get_template_part('snippets/homepage-philanthropy-block');
    }
  }
  ?>
</div>