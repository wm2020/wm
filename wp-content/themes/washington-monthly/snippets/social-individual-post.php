<!-- Social buttons -->
<div class="addthis_toolbox social" <?php get_template_part('snippets/addthis_vars') ?>>
  <div class="custom_images">

    <div class="action-buttons--first">
      <!--Donate-->

      <a class="btn btn-donate" href="https://donatenow.networkforgood.org/1407658" target=_"blank"
        alt="Make tax deductible donation with PayPal">
        <span class="fa fa-dollar"></span>
        <div>Donate</div>
      </a>

      <?php get_template_part('snippets/subscribe-button'); ?>
    </div>

    <!--Facebook-->
    <a class="addthis_button_facebook btn btn-facebook">
      <span class="fa fa-facebook"></span>
      <div>Share</div>
    </a>

    <!--Twitter-->
    <a class="addthis_button_twitter btn btn-twitter">
      <span class="fa fa-twitter"></span>
      <div>Tweet</div>
    </a>

    <!--Print-->
    <a class="addthis_button_print btn btn-print">
      <span class="fa fa-print"></span>
      <div>Print</div>
    </a>

    <!--Email-->
    <a class="addthis_button_email btn btn-email">
      <span class="fa fa-envelope"></span>
      <div>Email</div>
    </a>

  </div>
</div>