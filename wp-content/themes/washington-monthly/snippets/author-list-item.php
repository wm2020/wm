<h4 class="author-name"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>

<?php if($twitter = get_field('twitter_username', get_the_id())) : ?>
  <a href="https://twitter.com/<?php echo $twitter; ?>" class="link-twitter"></a>
<?php endif; ?>

<p class="bio"><?php echo html_entity_decode(wm_get_field('short_bio')) ?></p>