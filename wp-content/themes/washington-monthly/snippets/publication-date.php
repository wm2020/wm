<?php
$issues = get_the_terms($post, 'issues');
// Don't show date/time if post has an issue
if (!empty($issues)) {
    return;
}
?>

<div class="byline entry-meta vcard">
  <time class="updated entry-time"
        datetime="<?php echo get_the_time('m-d-Y') ?>"
        itemprop="datePublished">

    <?php echo get_the_time(get_option('date_format')) ?>

    <div class="timestamp">
      <?php if (!get_field('show_date_only')) : ?>
        | <?php echo get_the_time() ?>
      <?php endif; ?>
    </div>

  </time>
</div>
 