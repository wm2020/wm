<?php
$authors = get_field('author');
if(empty($authors)) return;

$label = (count($authors) > 1) ? "Authors" : "Author";

$original_post = $post; ?>
<div class="author-list">
  <?php
  foreach($authors as $author) {
    $post = $author;
    get_template_part('snippets/author-list-item');
  } ?>
</div>
<?php $post = $original_post;
