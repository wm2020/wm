<?php
$orig_post = $post;

$my_query = query_related_posts();
if(!$my_query || !$my_query->have_posts()) return;
?>

<div id="related-posts" class="clear">
  <h3>Related Posts</h3>
  <ul>
    <?php while($my_query->have_posts()) : $my_query->the_post(); ?>
    <li>
      <div class="related-post">
        <?php if(has_post_thumbnail()) : ?>
          <a rel="external" href="<? the_permalink()?>" class="related-thumb">
            <span>
              <?php the_post_thumbnail(array(150,150)); ?>
            </span>
          </a>
        <?php endif; ?>
        <div class="related-content">
          <a rel="external" href="<? the_permalink()?>" class="related-post-title">
            <?php the_display_head(); ?>
          </a>
          <div class="meta">
            <?php get_template_part('snippets/byline-author-list') ?>
            <?php get_template_part('snippets/publication-date') ?>
          </div>
          <div class="related-excerpt">
            <?php
            the_excerpt_max_charlength(100);
            ?>
          </div>
        </div>
      </div>
    </li>
    <?php endwhile; ?>
  </ul>
</div>

<?php $post = $orig_post; ?>
