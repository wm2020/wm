<!-- Social buttons -->
<div class="addthis_toolbox social">
  <div class="custom_images">

    <!--Facebook-->
    <a class="btn btn-facebook" href="https://www.facebook.com/WashingtonMonthly/" target="_blank">
      <span class="fa fa-facebook"></span>
      <div>Like</div>
    </a>

    <!--Twitter-->
    <a class="btn btn-twitter" href="https://twitter.com/monthly" target="_blank">
      <span class="fa fa-twitter"></span>
      <div>Follow</div>
    </a>

    <!--Donate-->

    <a class="btn btn-donate" href="https://donatenow.networkforgood.org/1407658" target=_"blank"
      alt="Make tax deductible donation with PayPal">
      <span class="fa fa-dollar"></span>
      <div>Donate</div>
    </a>

    <?php get_template_part('snippets/subscribe-button'); ?>

  </div>
</div>