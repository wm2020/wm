<?php if(has_post_thumbnail()) : ?>
<div class="featured-image">
  <?php the_post_thumbnail( 'bones-thumb-full' ); ?>
  <?php if ( $caption = get_post( get_post_thumbnail_id() )->post_excerpt ) : ?>
  <div class="caption"><?php echo $caption; ?></div>
  <?php endif; ?>
</div>
<?php endif; ?>
