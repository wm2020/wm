<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

  <section class="entry-content cf">
    <?php get_template_part('snippets/post-featured-image') ?>
  </section>

	<header class="entry-header article-header">
		<h3 class="h2 entry-title">
      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

      <?php get_template_part('snippets/byline-author-list') ?>
      <?php get_template_part('snippets/publication-date') ?>
	</header>

	<section class="entry-content cf">
		<?php the_excerpt() ?>
	</section>

	<footer class="article-footer">
	</footer>
</article>