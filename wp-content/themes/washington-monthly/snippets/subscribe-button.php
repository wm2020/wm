<!-- A/B test for subscribe modal -->
<?php 
    $randomId = array(
        'btnSubscribe-A', 
        'btnSubscribe-B'
    );

    $rand_keys = array_rand($randomId, 1);
?>
<a class="btn btn-subscribe" id="<?php echo($randomId[$rand_keys]); ?>">
    <span class="fa fa-envelope"></span>
    <div>Subscribe</div>
</a> 