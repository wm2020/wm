<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

	<header class="entry-header article-header">
		<h3 class="h2 entry-title">
      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

    <?php get_template_part('snippets/byline-author-list') ?>

    <div class="header-tag title--category header--magazine">
      <span>cover</span>
    </div>

    <?php get_template_part('snippets/publication-date') ?>
	</header>

	<section class="entry-content cf">
		<?php the_excerpt() ?>
	</section>

	<footer class="article-footer">
	</footer>
</article>