<div class="highlighted-category-block <?php if($sticky_post->have_posts()): ?>has-sticky<?php endif; ?>">
  <h2 class="<?php echo strtolower(str_replace(' ', '-', $category->name)) ?>">
    <a href="<?php echo esc_url($category_url); ?>"><?php echo $category->name ?></a>
  </h2>

  <?php // Single sticky post with image - this might not exist for some cats ?>
  <?php if($sticky_post->have_posts()) : $sticky_post->the_post(); ?>
  <div class="sticky-post">
    <?php if(has_post_thumbnail()) : ?>
    <div class="featured-image">
      <a href="<?php the_permalink() ?>"><?php the_post_thumbnail( 'featured-thumb' ); ?></a>
    </div>
    <?php endif; ?>

    <h3><a href="<?php the_permalink() ?>"><span><?php the_display_head(); ?></span></a></h3>
    <div class="meta">
      <?php get_template_part('snippets/byline-author-list') ?>
      <?php get_template_part('snippets/publication-date') ?>
    </div>
    <?php if(has_excerpt()): ?>
          <div style="clear: both;">
          <?php the_excerpt() ?>
          </div>
    <?php endif; ?>
  </div>
  <?php endif; ?>

  <?php // List of 5 highlighted posts ?>
  <ul class="highlighted-posts">
    <?php while($highlighted_posts->have_posts()) : ?>
    <?php $highlighted_posts->the_post(); ?>
    <li>
      <h3><a href="<?php the_permalink() ?>"><span><?php the_display_head(); ?></span></a></h3>
      <div class="meta">
        <?php get_template_part('snippets/byline-author-list') ?>
        <?php get_template_part('snippets/publication-date') ?>
      </div>
    </li>
    <?php endwhile; ?>
  </ul>
</div>