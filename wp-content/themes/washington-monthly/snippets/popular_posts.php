<?php

$orig_post = $post;

$my_query = query_most_popular_posts(5, 7);
if(!$my_query || !$my_query->have_posts()) return;

add_filter('excerpt_more', create_function('$more', 'return "...";'));
add_filter('excerpt_length', create_function('$length', "return 10;"));

?>
<div class="sidebar-feed widget">
	<h4 class="widgettitle">Most Popular</h4>
	<div class="upw-posts hfeed">

		<?php while($my_query->have_posts()) : $my_query->the_post(); ?>

			<article class="post type-post status-publish format-standard hentry">
				<header>
					<?php get_template_part('snippets/tag-header'); ?>

					<h4 class="entry-title">
						<a href="<?php the_permalink(); ?>" rel="bookmark">
							<?php the_title(); ?>
						</a>
					</h4>

					<?php get_template_part('snippets/byline-author-list'); ?>
					<?php get_template_part('snippets/publication-date'); ?>
				</header>

				<div class="entry-summary">
					<p>
						<a href="<?php the_permalink(); ?>">
							<?php echo get_the_excerpt(); ?>
						</a>
					</p>
				</div>

			</article>

		<?php endwhile; ?>
	</div>
</div>

<?php $post = $orig_post; ?>