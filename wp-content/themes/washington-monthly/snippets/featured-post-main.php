<?php
  $query = query_main_featured_post();

  // skip the rest of script if there is no featured post
  if (!$query->have_posts()) {
      return;
  }

  $query->the_post();

  // skip the rest of script if there is no thumbnail
  if (!has_post_thumbnail()) {
      return;
  }
?>


<div class="featured-blur"></div>
<div class="home-featured wrap">
  <div class="featured-image-wrapper">

    <div class="featured-image">
      <?php
      $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'featured', false);
      $bg_url = $src[0];
      ?>

      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
        alt="<?php the_title();?>" style='background-image: url(<?php echo $bg_url; ?> );'></a>

      <?php if ($caption = get_post(get_post_thumbnail_id())->post_excerpt) : ?>
        <div class="caption"><?php echo $caption; ?></div>
      <?php endif; ?>
    </div>

    <div class="featured-excerpt">
      <h2><a href="<?php the_permalink(); ?>" title="<?php the_title();?>" alt="<?php the_title();?>"><span><?php the_display_head(); ?></span></a></h2>
      <?php the_excerpt(); ?>
      <div class="meta">
        <?php get_template_part('snippets/byline-author-list') ?>
        <?php get_template_part('snippets/publication-date') ?>
      </div>
    </div>

    <?php get_template_part('snippets/homepage-recent-posts') ?>

  </div>
</div>
