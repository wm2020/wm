<ul class="recent-posts">
<h2><a href="/political-animal">Political Animal</a></h2>
  <?php $query = query_recent_posts(); ?>

  <?php $original_post = $post; ?>

  <?php while($query->have_posts()) : $query->the_post(); ?>
  <li>
    <a href="<?php the_permalink() ?>"><span><?php the_display_head(); ?></span></a>
    <div class="meta">
      <?php get_template_part('snippets/byline-author-list') ?>
      <?php get_template_part('snippets/publication-date') ?>
    </div>
  </li>
  <?php endwhile; ?>

  <?php $post = $original_post; ?>
</ul>
