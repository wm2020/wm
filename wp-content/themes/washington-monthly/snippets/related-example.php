<div id="related-posts" class="clear">
  <h3>Related Posts</h3>
  <ul>
        <li>
      <div class="related-post">
      <a rel="external" href="<? the_permalink()?>" class="related-thumb">
          <img src="http://placehold.it/150x100"/>
      </a>
                <div class="related-content">

          <a rel="external" href="http://localhost/2015/11/24/reminding-voters-what-works/" class="related-post-title">
            Reminding Voters What Works          </a>
          <div class="meta">
            <div class="author-info">by <a href="http://localhost/people/nancy-letourneau/">Nancy LeTourneau</a>

  <a href="https://twitter.com/nance" class="link-twitter">@nance</a>
</div>            
<div class="byline entry-meta vcard">
  <time class="updated entry-time" datetime="2015-11-24" itemprop="datePublished">

    November 24, 2015
          8:08 AM      </time>
</div>
          </div>
          <div class="related-excerpt">
            There has been a lot of pontificating about why Americans these days are so angry and <span class="read-more">...</span><span class="btn">read more</span>          </div>
        </div>
      </div>
    </li>
        <li>
      <div class="related-post">
                <div class="related-content">
          <a rel="external" href="http://localhost/2004/04/05/new-source-review/" class="related-post-title">
            New Source Review          </a>
          <div class="meta">
            <div class="author-info">by <a href="http://localhost/people/kevin-drum/">Kevin Drum</a>

</div>            
<div class="byline entry-meta vcard">
  <time class="updated entry-time" datetime="2004-04-05" itemprop="datePublished">

    April 5, 2004
          3:55 PM      </time>
</div>
          </div>
          <div class="related-excerpt">
            NEW SOURCE REVIEW….Ah, more of the open mindedness I’ve come to expect from the <span class="read-more">...</span><span class="btn">read more</span>          </div>
        </div>
      </div>
    </li>
        <li>
      <div class="related-post">
      <a rel="external" href="<? the_permalink()?>" class="related-thumb">
          <img src="http://placehold.it/150x100"/>
      </a>
                <div class="related-content">
          <a rel="external" href="http://localhost/2014/11/05/for-obama-no-point-in-being-conciliatory-now/" class="related-post-title">
            For Obama, No Point In Being Conciliatory Now          </a>
          <div class="meta">
            <div class="author-info">by <a href="http://localhost/people/ed-kilgore/">Ed Kilgore</a>

</div>            
<div class="byline entry-meta vcard">
  <time class="updated entry-time" datetime="2014-11-05" itemprop="datePublished">

    November 5, 2014
          11:42 AM      </time>
</div>
          </div>
          <div class="related-excerpt">
            I didn’t watch much TV last night, but I got the impression that whenever the gabbers ran <span class="read-more">...</span><span class="btn">read more</span>          </div>
        </div>
      </div>
    </li>
      </ul>
</div>