<!DOCTYPE html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); body_class(); ?> class="no-js">
<!--<![endif]-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta charset="utf-8">

    <?php // force Internet Explorer to use the latest rendering engine available ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php wp_title(''); ?></title>

    <?php // mobile meta (hooray!) ?>
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon-wm.png">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon-wm.png">
    <!--[if IE]>
      <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon-wm.ico">
    <![endif]-->
    <?php // or, set /favicon.ico for IE10 win ?>
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage"
        content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon-wm.png">
    <meta name="theme-color" content="#121212">

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-W3RQ528');
    </script>
    <!-- End Google Tag Manager -->

    <?php // wordpress head functions ?>
    <?php wp_head(); ?>
    <?php echo wm_get_ad_footer_markup() ?>
    <?php // end of wordpress head ?>
    <!-- Taboola -->
    <script type="text/javascript">
        window._taboola = window._taboola || [];
        _taboola.push({
            article: 'auto'
        });
        ! function (e, f, u, i) {
            if (!document.getElementById(i)) {
                e.async = 1;
                e.src = u;
                e.id = i;
                f.parentNode.insertBefore(e, f);
            }
        }(document.createElement('script'),
            document.getElementsByTagName('script')[0],
            '//cdn.taboola.com/libtrc/washingtonmonthly-network/loader.js',
            'tb_loader_script');
        if (window.performance && typeof window.performance.mark == 'function') {
            window.performance.mark('tbl_ic');
        }
    </script>
</head>

<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3RQ528" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="container">

        <header class="header <?php if ( is_front_page() || is_category() ) : ?>home-header<?php endif; ?>"
            role="banner" itemscope itemtype="http://schema.org/WPHeader">

            <div id="inner-header" class="wrap cf">

                <section class="search">
                    <?php get_search_form(); ?>
                </section>

                <nav role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement" class="wrap">
                    <?php wp_nav_menu(array(
	      'container' => false,                           // remove nav container
	      'container_class' => 'menu cf',                 // class of container (should you choose to use it)
	      'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name
	      'menu_class' => 'nav top-nav cf',               // adding custom nav class
	      'theme_location' => 'main-nav',                 // where it's located in the theme
	      'before' => '',                                 // before the menu
	      'after' => '',                                  // after the menu
	      'link_before' => '',                            // before each link
	      'link_after' => '',                             // after each link
	      'depth' => 0,                                   // limit the depth of the nav
	      'fallback_cb' => ''                             // fallback function (if there is one)
	    )); ?>

                </nav>

                <a href="<?php echo home_url(); ?>" rel="nofollow" id="logo">
                    <img src="/wp-content/themes/washington-monthly/library/images/wm-logo.svg"
                        class="svg white-alt bg--black" alt="Washington Monthly" style="display:none" ; />
                </a>

                <?php if ( is_front_page() ) : ?>
                <?php get_template_part('snippets/social-sitewide'); ?>
                <?php else : ?>
                <?php get_template_part('snippets/social-individual-post'); ?>
                <?php endif; ?>

            </div>

        </header>