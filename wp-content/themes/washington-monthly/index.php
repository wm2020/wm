<?php get_header(); ?>
<div id="content">
  <?php get_template_part('snippets/homepage-featured') ?>

  <div id="inner-content" class="wrap cf">
    <div id="main" class="m-all d-all cf" role="main">
      <div class="d-3of4 m-3of4" id="homepage-category-blocks">

	<div class="featured-posts">

		<?php if ( is_active_sidebar( 'homepage-top' ) ) : ?>
			<?php dynamic_sidebar( 'homepage-top' ); ?>
		<?php endif; ?>

	  <?php $query = query_more_featured_posts(); ?>
	  <?php while ($query->have_posts()) : $query->the_post(); ?>
	    <?php if(!has_post_thumbnail()) continue; ?>

	    <div class="featured-post-tile">
	      <?php get_template_part('snippets/tag-header') ?>

	      <div class="featured-image">
		<?php $src = wp_get_attachment_image_src(
		  get_post_thumbnail_id($post->ID), 'featured-thumb', false, '' ); ?>

		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
		   alt="<?php the_title();?>" style='background-image: url(<?php echo $src[0]; ?> );'></a>
	      </div>

	      <h2>
		<a href="<?php the_permalink(); ?>">
		  <?php the_display_head(); ?>
		</a>
	      </h2>
	      <div class="meta">
		<?php get_template_part('snippets/byline-author-list') ?>
		<?php get_template_part('snippets/publication-date') ?>
	      </div>

	      <?php the_excerpt(); ?>
	    </div>

	  <?php endwhile; ?>
	</div>

	<?php get_template_part('snippets/homepage-category-blocks'); ?>
      </div>
      <div class="aside d-1of4 m-1of4" id="sidebar">
	<?php get_template_part('snippets/sidebar-inner') ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
