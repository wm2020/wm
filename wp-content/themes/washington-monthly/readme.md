# Washington Monthly Theme


## Local front-end setup

*Requirements*
- NPM (https://nodejs.org/en/)
- Grunt (run 'npm install -g grunt-cli')

*Setup Local Environment
- cd into theme dir (this dir)
- run 'npm install grunt --save-dev' to locally install node modules
- run 'grunt watch' to poll for js and scss changes

Write your sass and js in library/scss/* and library/js/* respectively.