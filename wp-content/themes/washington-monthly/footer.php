</div>
<!-- End of #container -->
<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

	<div id="inner-footer" class="wrap cf">
		<a href="<?php echo home_url(); ?>" rel="nofollow" id="logo">
			<img src="/wp-content/themes/washington-monthly/library/images/wm-logo.svg" class="svg white logo--noreg" alt="Washington Monthly"/>
		</a>

		<?php get_template_part('snippets/newsletter'); ?>

		<div class="action-buttons">
			<!--Donate-->

			<a class="btn btn-donate" href="https://donatenow.networkforgood.org/1407658" target=_"blank"
				alt="Make tax deductible donation with PayPal">
				<span class="fa fa-dollar"></span>
				<div>Donate</div>
			</a>

			<?php get_template_part('snippets/subscribe-button'); ?>
		</div>

		<nav role="navigation">
			<?php wp_nav_menu(array(
				'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
				'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
				'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
				'menu_class' => 'nav footer-nav cf',            // adding custom nav class
				'theme_location' => 'footer-links',             // where it's located in the theme
				'before' => '',                                 // before the menu
				'after' => '',                                  // after the menu
				'link_before' => '',                            // before each link
				'link_after' => '',                             // after each link
				'depth' => 0,                                   // limit the depth of the nav
				'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
			)); ?>
		</nav>

		<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>

	</div>

</footer>

<?php // all js scripts are loaded in library/bones.php ?>
<script>
	var $ = jQuery;
</script>
<?php wp_footer(); ?>

<script id="dsq-count-scr" src="https://washingtonmonthly.disqus.com/count.js" async></script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56ebddc96a2efb87"></script>

<script>
	window.twttr = (function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0],
			t = window.twttr || {};
		if (d.getElementById(id)) return t;
		js = d.createElement(s);
		js.id = id;
		js.src = "https://platform.twitter.com/widgets.js";
		fjs.parentNode.insertBefore(js, fjs);

		t._e = [];
		t.ready = function (f) {
			t._e.push(f);
		};

		return t;
	}(document, "script", "twitter-wjs"));
</script>

<script>
	(function (i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function () {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '/wp-content/themes/washington-monthly/build/js/analytics.min.js', 'ga');

	ga('create', 'UA-77181581-1', 'auto');
	ga('send', 'pageview');
</script>

<script type="text/javascript">
	window._taboola = window._taboola || [];
	_taboola.push({
		flush: true
	});
</script>

<!-- Begin Constant Contact Active Forms -->
<script> var _ctct_m = "9fac7c46f337cb9a44b53a935f5634f7"; </script>
<script id="signupScript" src="//static.ctctcdn.com/js/signup-form-widget/current/signup-form-widget.min.js" async defer></script>
<!-- End Constant Contact Active Forms -->
</body>

</html>