module.exports = function(grunt) {
  grunt.initConfig({
    uglify: {
      scripts: {
        expand: true,
        cwd: "library/js/",
        src: "**.js",
        dest: "build/",
        ext: ".min.js"
      },
      options: {
        livereload: true
      }
    },

    sass: {
      // Task
      dist: {
        // Target
        options: {
          // Target options
          style: "compressed"
        },
        files: {
          // Dictionary of files
          "library/css/style.css": "library/scss/style.scss",
          "css/editor.css": "library/scss/editor-style.scss"
        }
      }
    },

    concat: {
      options: {
        separator: "\n\n",
        livereload: true
      },
      dist: {
        src: ["build/*.min.js"],
        dest: "build/js/app.min.js"
      }
    },

    watch: {
      css: {
        files: "library/scss/**/*.scss",
        tasks: ["sass"],
        options: {
          livereload: false
        }
      }

      // files: ['library/js/**/*.js'],
      // tasks: ['uglify','concat'],

      // options:{
      //   livereload:true,
      // },

      // php: {
      //     files: ['**/*.php'],
      //     options: {
      //       livereload: 35729
      //     }
      //   }
    }
  });

  grunt.loadNpmTasks("grunt-contrib-jshint");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-watch");

  grunt.registerTask("default", ["uglify", "sass", "concat"]);
  grunt.registerTask("build", ["jshint", "uglify", "scss", "concat"]);
};
