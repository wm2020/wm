<form role="search" method="get" id="searchform" class="searchform search-form" action="<?php echo home_url( '/' ); ?>">
    <div class="form-group has-feedback">
        <!-- <label for="s" class="screen-reader-text"><?php _e('Search for:','bonestheme'); ?></label> -->
        <input type="search" id="s" name="s" class="form-control" placeholder="Search Washington Monthly..." value="" />
        <button type="submit" id="searchsubmit" class="hidden" ><?php _e('','bonestheme'); ?></button>
        <span class="fa fa-search form-control-feedback"></span>
    </div>
</form>