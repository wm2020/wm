<?php
/*
 * CUSTOM POST TYPE TEMPLATE
 *
 * This is the custom post type post template. If you edit the post type name, you've got
 * to change the name of this template to reflect that name change.
 *
 * For Example, if your custom post type is "register_post_type( 'bookmarks')",
 * then your single template should be single-bookmarks.php
 *
 * Be aware that you should rename 'custom_cat' and 'custom_tag' to the appropiate custom
 * category and taxonomy slugs, or this template will not finish to load properly.
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates
*/
?>
<?php get_header(); ?>
<div id="content">
    

    <div id="inner-content" class="wrap cf">

        <main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage"
            itemtype="http://schema.org/Blog">

            <?php get_template_part('snippets/ads/top-banner'); ?>

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>



                <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">

                    <header class="article-header">
                        <h1 class="single-title custom-post-type-title"><?php the_title(); ?></h1>
                    </header>

                    <section class="entry-content cf">

                        <!-- CUSTOM FIELD: HEADSHOT -->
                        <?php if ($headshot = get_field('headshot')): ?>
                            <img src="<?php echo $headshot['url']; ?>" class="headshot" />
                        <?php endif; ?>


                        <!-- CUSTOM FIELD: TWITTER USERNAME -->
                        <?php if ($twitter = get_field('twitter_username')): ?>
                            <div class="link-twitter-wrapper">
                                Follow <?php echo strtok(get_the_title(), " "); ?> <a
                                    href="https://twitter.com/<?php echo $twitter; ?>" class="link-twitter">
                                    @<?php echo $twitter; ?></a>
                            </div>
                        <?php endif; ?>

                        <?php
                            the_content();
                            get_template_part('snippets/posts-by-current-author');
                        ?>
                    </section> <!-- end article section -->

                    <footer class="article-footer">
                        <p class="tags">
                            <?php echo get_the_term_list(get_the_ID(), 'custom_tag', '<span class="tags-title">' . __('Custom Tags:', 'bonestheme') . '</span> ', ', ') ?>
                        </p>

                    </footer>

                    <?php comments_template(); ?>

                </article>

            <?php endwhile; ?>

            <?php else : ?>

                <article id="post-not-found" class="hentry cf">
                    <header class="article-header">
                        <h1><?php _e('Oops, Post Not Found!', 'bonestheme'); ?></h1>
                    </header>
                    <section class="entry-content">
                        <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'bonestheme'); ?></p>
                    </section>
                    <footer class="article-footer">
                        <p><?php _e('This is the error message in the single-custom_type.php template.', 'bonestheme'); ?>
                        </p>
                    </footer>
                </article>

            <?php endif; ?>

        </main>

        <?php get_sidebar(); ?>

    </div>

</div>

<?php get_footer(); ?>