<?php
/*
* CUSTOM POST TYPE TAXONOMY TEMPLATE
*
* This is the custom post type taxonomy template. If you edit the custom taxonomy name,
* you've got to change the name of this template to reflect that name change.
*
* For Example, if your custom taxonomy is called "register_taxonomy('shoes')",
* then your template name should be taxonomy-shoes.php
*
* For more info: http://codex.wordpress.org/Post_Type_Templates#Displaying_Custom_Taxonomies
*/
?>
<?php $term = get_queried_object();  ?>
<?php get_header(); ?>

<div id="content">

    <div id="inner-content" class="wrap cf">

        <main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage"
            itemtype="http://schema.org/Blog">

            <?php get_template_part('snippets/ads/top-banner'); ?>

            <h1 class="page-title h1"><?php single_cat_title(); ?></h1>

            <?php $custom_query = query_issue_cover($term->term_id); ?>
            <?php if ($custom_query->have_posts()) : ?>
            <?php while ($custom_query->have_posts()) : ?>
            <?php $custom_query->the_post(); ?>
            <?php get_template_part('snippets/post-listing-cover-item') ?>
            <?php endwhile; ?>
            <?php endif; ?>

            <?php $custom_query = query_issue_archive($term->term_id); ?>
            <?php if ($custom_query->have_posts()) : ?>
            <?php while ($custom_query->have_posts()) : ?>
            <?php $custom_query->the_post(); ?>
            <?php get_template_part('snippets/post-listing-item') ?>
            <?php endwhile; ?>
            <?php else : ?>
            <article id="post-not-found" class="hentry cf">
                <section class="entry-content">
                    <p>There are currently no posts in this category.</p>
                </section>
            </article>
            <?php endif; ?>
        </main>

        <?php get_sidebar(); ?>

    </div>
</div>

<?php get_footer(); ?>