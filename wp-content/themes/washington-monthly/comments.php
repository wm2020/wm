<?php
/*
* Don't show comments if they aren't enabled or
* the post requires a password to view
*/
if (!comments_open() || post_password_required()) return;
?>

<div id="disqus_thread"></div>
<script>
var disqus_shortname = 'washingtonmonthly';
var disqus_config = function () {
  this.page.identifier = '<?php the_disqus_id() ?>';
};

(function($) {
		$(document).ready(function() {
//    $(window).load(function() {
        $.ajax({
            type: "GET",
            url: "https://" + disqus_shortname + ".disqus.com/embed.js",
            dataType: "script",
            cache: true
          });
      });
})(jQuery);
</script>

<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>