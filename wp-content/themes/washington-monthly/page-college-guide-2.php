<?php
/*
Template Name: College Guide 2
 */
?>
<?php get_header(); ?>
<?php get_template_part('snippets/ads/top-banner-college'); ?>
<div id="content">

  <div id="inner-content" class="wrap cf">

    <main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

	<header class="article-header">

	  <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>

	  <p class="byline vcard">
	    <?php printf( __( 'Posted', 'bonestheme').' <time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time> '.__( 'by',  'bonestheme').' <span class="author">%3$s</span>', get_the_time('Y-m-j'), get_the_time(get_option('date_format')), get_the_author_link( get_the_author_meta( 'ID' ) )); ?>
	  </p>

	</header> <?php // end article header ?>

	<section class="entry-content cf" itemprop="articleBody">

	  <?php get_template_part('snippets/post-featured-image') ?>
	  <?php get_template_part('snippets/social-individual-post') ?>
	  <?php the_content() ?>

	</section> <?php // end article section ?>
      </article>

      <?php endwhile; endif; ?>

    </main>

    <?php get_sidebar('college-guide'); ?>

  </div>

</div>

<?php get_footer(); ?>
