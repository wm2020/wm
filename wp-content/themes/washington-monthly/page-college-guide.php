<?php
/*
Template Name: College Guide
 */
?>

<?php get_header(); ?>
<div id="content">

    <div id="inner-content" class="wrap cf">


        <?php
    $show_sidebar = true;
    $body_classes = "t-2of3 d-5of7";
    ?>


        <main id="main" class="m-all <?php echo $body_classes; ?> cf" role="main" itemscope itemprop="mainContentOfPage"
            itemtype="http://schema.org/Blog">

            <?php get_template_part('snippets/ads/top-banner'); ?>

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope
                itemtype="http://schema.org/BlogPosting">

                <header class="article-header">
                    <?php if (has_post_thumbnail()) : ?>
                    <?php the_post_thumbnail('full'); ?>
                    <?php else: ?>
                    <h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>

                    <?php endif; ?>
                </header> <?php // end article header?>

                <section class="entry-content cf" itemprop="articleBody">
                    <?php
              the_content();

              wp_link_pages(array(
                  'before'      => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'bonestheme') . '</span>',
                  'after'       => '</div>',
                  'link_before' => '<span>',
                  'link_after'  => '</span>',
                ));
                        ?>

                    <?php dynamic_sidebar('content-bottom'); ?>

                </section> <?php // end article section?>

                <?php comments_template(); ?>

            </article>

            <?php endwhile; endif; ?>

        </main>

        <?php get_sidebar(); ?>

    </div>

</div>

<?php get_footer(); ?>