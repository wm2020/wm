<?php
/*
 Template Name: Style Guide
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
							<?php get_template_part('snippets/ads/top-banner'); ?>

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

									<h1 class="page-title"><?php the_title(); ?></h1>

								</header>

								<section class="entry-content cf" itemprop="articleBody">
									<?php
                                        // the content
                                        the_content();
                                    ?>
									<!-- Start style columns (2 sections) -->
									<div class="d-1of2 t-1of2">
										<div id="logos" class="panel">
											<h4>Logos</h4>
											<!--Logos-->
											<div class="logo--large">
												<header>
													<div id="mega-menu-wrap-main-nav" class="mega-menu-wrap">
														<div class="mega-menu-toggle">
															<div class="mega-toggle-block mega-menu-toggle-block mega-toggle-block-right" id="mega-toggle-block-1">
															</div>
														</div>
													</div>
												</header>
												<img src="/wp-content/themes/washington-monthly/library/images/wm-logo.svg" class="svg bg--black white-alt" alt="Washington Monthly" />

											</div>

											<h5 class="sans-serif">No registered mark</h5>
											<div class="logo--noreg">
												<img src="/wp-content/themes/washington-monthly/library/images/wm-logo.svg" class="img--small svg alt="Washington Monthly""/>
												<img src="/wp-content/themes/washington-monthly/library/images/wm-logo.svg" class="img--small color svg" alt="Washington Monthly"/>
											</div>

											<h5 class="sans-serif">Smaller format</h5>
											<div class="logo--small">
												<img src="/wp-content/themes/washington-monthly/library/images/wm-logo.svg" class="img--small svg black" alt="Washington Monthly"/>
											</div>
										</div>

										<div id="colors" class="panel">

											<h4>Color Palette</h4>

											<div class="swatch bg--primary"></div>
											<div class="swatch bg--primaryAlt"></div><br/>
											<div class="swatch bg--secondary"></div>
											<div class="swatch bg--secondaryAlt"></div>
											<div class="swatch bg--secondaryAltLight"></div><br/>
											<div class="swatch highlight"></div>
											<div class="swatch highlightAlt"></div>
											<div class="swatch success"></div>
											<div class="swatch bg--alert"></div><br/>
											<div class="swatch bg--black"></div>
											<div class="swatch bg--grey"></div>
											<div class="swatch bg--lightGrey"></div>
											<div class="swatch bg--lighterGrey"></div>
											<div class="swatch bg--lightestGrey"></div>

										</div>

									</div>

									<div class="d-1of2 t-1of2">

										<div id="icons" class="panel">
											<h4>Icons</h4>

											<i class="fa fa-facebook"></i>
											<i class="fa fa-twitter"></i>
											<i class="fa fa-envelope-o"></i>
											<i class="fa fa-rss"></i>
											<i class="fa fa-share"></i>
											<i class="fa fa-reply"></i>
											<i class="fa fa-reply-all"></i>
											<i class="fa fa-map-marker"></i>
											<i class="fa fa-map"></i>
											<i class="fa fa-microphone"></i>
											<i class="fa fa-anchor"></i>
											<i class="fa fa-angle-down"></i>
											<i class="fa fa-angle-left"></i>
											<i class="fa fa-angle-right"></i>
											<i class="fa fa-angle-up"></i>
											<i class="fa fa-area-chart"></i>
											<i class="fa fa-arrow-circle-down"></i>
											<i class="fa fa-at"></i>
											<i class="fa fa-asterisk"></i>
											<i class="fa fa-bar-chart"></i>
											<i class="fa fa-bolt"></i>
											<i class="fa fa-bullhorn"></i>
											<i class="fa fa-calendar"></i>
											<i class="fa fa-calendar-check-o"></i>
											<i class="fa fa-camera-retro"></i>
											<i class="fa fa-link"></i>
											<i class="fa fa-chevron-right"></i>
											<i class="fa fa-circle"></i>
											<i class="fa fa-bars"></i>
											<i class="fa fa-close"></i>
											<i class="fa fa-comment"></i>
											<i class="fa fa-commenting-o"></i>
											<i class="fa fa-commenting-o"></i>
											<i class="fa fa-comments-o"></i>
											<i class="fa fa-desktop"></i>
											<i class="fa fa-external-link"></i>
											<i class="fa fa-exclamation-triangle"></i>
											<i class="fa fa-expand"></i>
											<i class="fa fa-eye"></i>
											<i class="fa fa-file-text-o"></i>
											<i class="fa fa-heart"></i>
											<i class="fa fa-i-cursor"></i>
											<i class="fa fa-mobile"></i>
											<i class="fa fa-tablet"></i>
											<i class="fa fa-pencil"></i>
											<i class="fa fa-paragraph"></i>
											<i class="fa fa-plus"></i>
											<i class="fa fa-plus-square-o"></i>
											<i class="fa fa-print"></i>
											<i class="fa fa-question-circle"></i>
											<i class="fa fa-quote-left"></i>
											<i class="fa fa-quote-right"></i>
											<i class="fa fa-retweet"></i>
											<i class="fa fa-rocket"></i>
											<i class="fa fa-save"></i>
											<i class="fa fa-bookmark"></i>
											<i class="fa fa-download"></i>
											<i class="fa fa-search"></i>
											<i class="fa fa-paper-plane"></i>
											<i class="fa fa-tags"></i>
											<i class="fa fa-th"></i>
											<i class="fa fa-th-large"></i>
											<i class="fa fa-list"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-pin"></i>
										</div>

										<div id="buttons" class="panel">

											<h4>Buttons</h4>

											<!-- Standard buttons -->
											<a class="btn btn--red">Standard button</a>
											<a class="btn btn--blue">Standard button</a>

											<hr/>

											<?php get_template_part('snippets/social-sitewide') ?>

											<?php get_template_part('snippets/social-individual-post') ?>

										</div>

										<div id="form-elements" class="panel">

											<h4>Form Elements</h4>

											<form id="styleguide-form">
												<input type="text" placeholder="Single line textfield"/>
												<input type="email" placeholder="email@email.com"/>
												<input type="tel" placeholder="(555) 555-5555" />

												<input type="text" class="d-1of2" style="width:49%; margin-right:2%;" placeholder="Single line textfield"/>
												<input type="text" class="d-1of2" style="width:49%;" placeholder="Single line textfield"/>

												<select name="select" form="styleguide-form">
													<option value="one">Option one</option>
													<option value="two">Option two</option>
													<option value="three">Option three</option>
												</select>

												<textarea value="textarea" placeholder="Multi-line input textfield"></textarea>

												<button type="submit">Submit</button>
											</form>

										</div>

									</div>

									<div class="d-all panel" id="comments">
										<h4>Comments</h4>

										<?php comments_template(); ?>

									</div>

									<div class="d-all">
										<div id="headers" class="panel">

											<div class="panel">

												<h4>Headers</h4>

												<div class="sans-serif d-1of2">
													<h1>H1 - Featured Title</h1>
													<h2>H2 - Sub headline</h2>
													<h3>H3 - Alternate sub headline</h3>
													<h4>H4 - In content headline</h4>
													<h5>H5 - Subtext highlight</h5>
													<h6>H6 - Footnote title/highlight</h6>
												</div>

												<div class="serif d-1of2">
													<h1>H1 - Featured Title</h1>
													<h2>H2 - Sub headline</h2>
													<h3>H3 - Alternate sub headline</h3>
													<h4>H4 - In content headline</h4>
													<h5>H5 - Subtext highlight</h5>
													<h6>H6 - Footnote title/highlight</h6>
												</div>
											</div>

											<div class="d-all panel">
												<div class="title--category sans-serif"><a href="#">Example category title</a></div>
												<div class="title--category header--mag sans-serif"><a href="#">The Magazine</a></div>
												<div class="title--category header--politics sans-serif"><a href="#">Politics</a></div>
												<div class="title--category header--education sans-serif"><a href="#">Education</a></div>
												<div class="title--category header--economy sans-serif"><a href="#">Economy</a></div>
												<div class="title--category header--gov sans-serif"><a href="#">Government</a></div>
												<div class="title--category header--health sans-serif"><a href="#">Health Care</a></div>
												<div class="title--category header--law sans-serif"><a href="#">Law and Justice</a></div>
												<div class="title--category header--books sans-serif"><a href="#">Books</a></div>
												<div class="title--category header--phil sans-serif"><a href="#">Successes of Philanthropy</a></div>
											</div>

										</div>


										<div id="paragraph" class="panel">

											<h4>Paragraph Text</h4>

											<div class="d-1of2">

												<div class="featured-image">
													<img src="http://placehold.it/650x450" />
													<span class="caption text--right">This is a right aligned caption/photo credit</span>
												</div>

												<p>A fifty-two-year old mother of three long-since-grown children, she grew up in Flint, Michigan—all four grandparents worked at the former GM plant there—and worked for most of her career as a nursing home aide. Several years ago, she and her husband, Marshall, moved to Pellston, a small village just south of the <a href="">Mackinac Bridge</a>, which leads to northern Michigan’s Upper Peninsula, to manage a Jet’s Pizza franchise.</p>
											</div>

											<div class="d-1of2">

												<div class="featured-image">
													<img src="http://placehold.it/650x450" />
													<span class="caption text--left">This is a left aligned caption/photo credit</span>
												</div>

												<p>A fifty-two-year old mother of three long-since-grown children, she grew up in Flint, Michigan—all four grandparents worked at the former GM plant there—and worked for most of her career as a nursing home aide. Several years ago, she and her husband, Marshall, moved to Pellston, a small village just south of the <a href="">Mackinac Bridge</a>, which leads to northern Michigan’s Upper Peninsula, to manage a Jet’s Pizza franchise.</p>

											</div>

										</div>

										<div class="panel">
											<h4>Blockquote styles</h4>

											<p class="quote">On election night in November 2014, Pierce was watching the returns, distressed less about <a href="">Michigan Democrats</a> losing (yet again) as by the state’s dismal voter turnout: just 41 percent of registered voters. “And then I read on the Internet about Oregon—and how much higher their turnout was. I decided to make..</p>

											<p><aside class="pullquote align-right">"And then I read on the Internet about Oregon—and how much higher their turnout was."</aside> A fifty-two-year old mother of three long-since-grown children, she grew up in Flint, Michigan—all four grandparents worked at the former GM plant there—and worked for most of her career as a nursing home aide. Several years ago, she and her husband, Marshall, moved to Pellston, a small village just south of the <a href="">Mackinac Bridge</a>, which leads to northern Michigan’s Upper Peninsula, to manage a Jet’s Pizza franchise.</p>
											<p>A fifty-two-year old mother of three long-since-grown children, she grew up in Flint, Michigan—all four grandparents worked at the former GM plant there—and worked for most of her career as a nursing home aide. Several years ago, she and her husband, Marshall, moved to Pellston, a small village just south of the <a href="">Mackinac Bridge</a>, which leads to northern Michigan’s Upper Peninsula, to manage a Jet’s Pizza franchise.</p>


										</div>
									</div>

									<div class="d-all">
										<div id="ads" class="panel">
											<h4>Ads</h4>

											<h5>Standard ad sizes</h5>
											<p><i>Note: not all possible ad sizes for all devices are represented. This is to illustrate standard sizes for use in layout design.</i></p>
											<img src="http://placehold.it/300x600"/>
											<img src="http://placehold.it/300x250"/>
											<img src="http://placehold.it/320x100"/>
											<img src="http://placehold.it/336x280"/>
											<img src="http://placehold.it/728x90"/>
											<hr>
											<h5>Other ad sizes</h5>
											<img src="http://placehold.it/320x50"/>
											<img src="http://placehold.it/234x60"/>
											<img src="http://placehold.it/468x60"/>
											<img src="http://placehold.it/970x90"/>
											<img src="http://placehold.it/970x250"/><br />
											<img src="http://placehold.it/300x1050"/>
											<img src="http://placehold.it/120x600"/>
											<img src="http://placehold.it/160x600"/>
											<img src="http://placehold.it/120x240"/>
											<img src="http://placehold.it/180x150"/>
											<img src="http://placehold.it/125x125"/>
											<img src="http://placehold.it/250x250"/>
											<img src="http://placehold.it/200x200"/>
										</div>

									</div>

									<div class="d-all">
										<div id="related" class="panel">
											<h4>Related Posts</h4>

											<?php get_template_part('snippets/related-example') ?>
										</div>
									</div>
								</section>


								<footer class="article-footer">

		  <?php the_tags('<p class="tags"><span class="tags-title">' . __('Tags:', 'bonestheme') . '</span> ', ', ', '</p>'); ?>

								</footer>

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e('Oops, Post Not Found!', 'bonestheme'); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'bonestheme'); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e('This is the error message in the page-custom.php template.', 'bonestheme'); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>

				</div>

			</div>



<?php get_footer(); ?>
