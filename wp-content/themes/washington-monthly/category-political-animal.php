<?php get_header(); ?>
<div id="content">
    

  <div id="inner-content" class="wrap cf">

    <main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

      <?php get_template_part('snippets/ads/top-banner'); ?>

      <?php
      $title = str_replace("Category: ", "", get_the_archive_title());
      echo "<h1 class='h1 title--category header--" . strtolower(str_replace(" ", "-", $title)) . "'>" . $title . "</h1>";

      the_archive_description( '<div class="taxonomy-description">', '</div>' );
      ?>

      <?php $lockerdome_included = false; ?>

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <?php get_template_part('snippets/post-listing-item-full') ?>

      <?php if (!$lockerdome_included) : ?>
      <?php	$lockerdome_included = true; ?>
      <?php get_template_part('snippets/ads/lockerdome'); ?>
      <?php endif; ?>
			<?php endwhile; ?>

      <?php bones_page_navi(); ?>

		<?php else : ?>

      <article id="post-not-found" class="hentry cf">
	<section class="entry-content">
	  <p>There are currently no posts in this category.</p>
	</section>
      </article>

      <?php endif; ?>

    </main>

    <?php get_sidebar(); ?>

  </div>

</div>

<?php get_footer(); ?>
